/** 
 * @file llfilepicker.cpp
 * @brief OS-specific file picker
 *
 * $LicenseInfo:firstyear=2001&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llfilepicker.h"
#include "llworld.h"
#include "llviewerwindow.h"
#include "llkeyboard.h"
#include "llviewercontrol.h"
#include "llwindow.h"	// beforeDialog()

#if LL_SDL
#include "llwindowsdl.h" // for some X/GTK utils to help with filepickers
#endif // LL_SDL

//
// Globals
//

LLFilePicker LLFilePicker::sInstance;

#include "llfilepicker_mac.h"

//
// Implementation
//
LLFilePicker::LLFilePicker()
	: mCurrentFile(0),
	  mLocked(false)

{
	reset();
	
	mPickOptions = 0;

}

LLFilePicker::~LLFilePicker()
{
	// nothing
}

// utility function to check if access to local file system via file browser 
// is enabled and if not, tidy up and indicate we're not allowed to do this.
bool LLFilePicker::check_local_file_access_enabled()
{
	// if local file browsing is turned off, return without opening dialog
	bool local_file_system_browsing_enabled = gSavedSettings.getBOOL("LocalFileSystemBrowsingEnabled");
	if ( ! local_file_system_browsing_enabled )
	{
		mFiles.clear();
		return false;
	}

	return true;
}

const std::string LLFilePicker::getFirstFile()
{
	mCurrentFile = 0;
	return getNextFile();
}

const std::string LLFilePicker::getNextFile()
{
	if (mCurrentFile >= getFileCount())
	{
		mLocked = false;
		return std::string();
	}
	else
	{
		return mFiles[mCurrentFile++];
	}
}

const std::string LLFilePicker::getCurFile()
{
	if (mCurrentFile >= getFileCount())
	{
		mLocked = false;
		return std::string();
	}
	else
	{
		return mFiles[mCurrentFile];
	}
}

void LLFilePicker::reset()
{
	mLocked = false;
	mFiles.clear();
	mCurrentFile = 0;
}

std::unique_ptr<std::vector<std::string>> LLFilePicker::navOpenFilterProc(ELoadFilter filter) //(AEDesc *theItem, void *info, void *callBackUD, NavFilterModes filterMode)
{
    std::unique_ptr<std::vector<std::string>> allowedv(new std::vector< std::string >);
    switch(filter)
    {
        case FFLOAD_ALL:
            allowedv->push_back("wav");
            allowedv->push_back("bvh");
            allowedv->push_back("anim");
            allowedv->push_back("dae");
            allowedv->push_back("raw");
            allowedv->push_back("lsl");
            allowedv->push_back("dic");
            allowedv->push_back("xcu");
            allowedv->push_back("oxp");
            allowedv->push_back("gif");
        case FFLOAD_IMAGE:
            allowedv->push_back("jpg");
            allowedv->push_back("jpeg");
            allowedv->push_back("bmp");
            allowedv->push_back("tga");
            allowedv->push_back("bmpf");
            allowedv->push_back("tpic");
            allowedv->push_back("png");
            break;
        case FFLOAD_EXE:
            allowedv->push_back("app");
            allowedv->push_back("exe");
            break;
        case FFLOAD_WAV:
            allowedv->push_back("wav");
            break;
        case FFLOAD_ANIM:
            allowedv->push_back("bvh");
            allowedv->push_back("anim");
            break;
        case FFLOAD_COLLADA:
            allowedv->push_back("dae");
            break;
        case FFLOAD_XML:
            allowedv->push_back("xml");
            break;
        case FFLOAD_RAW:
            allowedv->push_back("raw");
            break;
        case FFLOAD_SCRIPT:
            allowedv->push_back("lsl");
            break;
        case FFLOAD_DICTIONARY:
            allowedv->push_back("dic");
            allowedv->push_back("xcu");
            break;
        case FFLOAD_DIRECTORY:
            break;
case FFLOAD_IMPORT:
            allowedv->push_back("oxp");
	    break;
        default:
            LL_WARNS() << "Unsupported format." << LL_ENDL;
    }
	return allowedv;
}

bool	LLFilePicker::doNavChooseDialog(ELoadFilter filter)
{
	// if local file browsing is turned off, return without opening dialog
	if ( check_local_file_access_enabled() == false )
	{
		return false;
	}
    
	gViewerWindow->getWindow()->beforeDialog();
    
    std::unique_ptr<std::vector<std::string>> allowed_types = navOpenFilterProc(filter);
    
    std::unique_ptr<std::vector<std::string>> filev  = doLoadDialog(allowed_types.get(),
                                                    mPickOptions);

	gViewerWindow->getWindow()->afterDialog();


    if (filev && filev->size() > 0)
    {
        mFiles.insert(mFiles.end(), filev->begin(), filev->end());
        return true;
    }
	
	return false;
}

bool	LLFilePicker::doNavSaveDialog(ESaveFilter filter, const std::string& filename)
{
	
	// Setup the type, creator, and extension
    std::string		extension, type, creator;
    
	switch (filter)
	{
		case FFSAVE_WAV:
			type = "WAVE";
			creator = "TVOD";
			extension = "wav";
			break;
		case FFSAVE_TGA:
			type = "TPIC";
			creator = "prvw";
			extension = "tga";
			break;
		case FFSAVE_TGAPNG:
			type = "PNG";
			creator = "prvw";
			extension = "png,tga,jpg,jpeg,j2c,bmp";
			break;
		case FFSAVE_BMP:
			type = "BMPf";
			creator = "prvw";
			extension = "bmp";
			break;
		case FFSAVE_JPEG:
			type = "JPEG";
			creator = "prvw";
			extension = "jpeg";
			break;
		case FFSAVE_PNG:
			type = "PNG ";
			creator = "prvw";
			extension = "png";
			break;
		case FFSAVE_AVI:
			type = "\?\?\?\?";
			creator = "\?\?\?\?";
			extension = "mov";
			break;
		case FFSAVE_ANIM:
			type = "\?\?\?\?";
			creator = "\?\?\?\?";
			extension = "xaf";
			break;
			
		case FFSAVE_XML:
			type = "\?\?\?\?";
			creator = "\?\?\?\?";
			extension = "xml";
			break;
			
		case FFSAVE_RAW:
			type = "\?\?\?\?";
			creator = "\?\?\?\?";
			extension = "raw";
			break;

		case FFSAVE_J2C:
			type = "\?\?\?\?";
			creator = "prvw";
			extension = "j2c";
			break;
		
		case FFSAVE_SCRIPT:
			type = "LSL ";
			creator = "\?\?\?\?";
			extension = "lsl";
			break;
		case FFSAVE_EXPORT:
			type = "OXP ";
			creator = "\?\?\?\?";
			extension = "oxp";
			break;
		case FFSAVE_COLLADA:
			type = "DAE ";
			creator = "\?\?\?\?";
			extension = "dae";
			break;
		case FFSAVE_DAE:
			type = "DAE ";
			creator = "\?\?\?\?";
			extension = "dae";
			break;
		case FFSAVE_ALL:
		default:
			type = "\?\?\?\?";
			creator = "\?\?\?\?";
			extension = "";
			break;
	}
	
    std::string namestring = filename;
    if (namestring.empty()) namestring="Untitled";
    
//    if (! boost::algorithm::ends_with(namestring, extension) )
//    {
//        namestring = namestring + "." + extension;
//        
//    }
    
	gViewerWindow->getWindow()->beforeDialog();

	// Run the dialog
    std::unique_ptr<std::string> filev = doSaveDialog(&namestring, 
                 &type,
                 &creator,
                 &extension,
                 mPickOptions);

	gViewerWindow->getWindow()->afterDialog();

	if ( filev && !filev->empty() )
	{
        mFiles.push_back(*filev);
		return true;
    }
	
	return false;
}

bool LLFilePicker::getOpenFile(ELoadFilter filter, bool blocking)
{
	if( mLocked )
		return false;

	bool success = false;

	// if local file browsing is turned off, return without opening dialog
	if ( check_local_file_access_enabled() == false )
	{
		return false;
	}

	reset();
	
    mPickOptions &= ~F_MULTIPLE;
    mPickOptions |= F_FILE;
 
    if (filter == FFLOAD_DIRECTORY) //This should only be called from lldirpicker. 
    {

        mPickOptions |= ( F_NAV_SUPPORT | F_DIRECTORY );
        mPickOptions &= ~F_FILE;
    }

	if (filter == FFLOAD_ALL)	// allow application bundles etc. to be traversed; important for DEV-16869, but generally useful
	{
        mPickOptions |= F_NAV_SUPPORT;
	}
	
	if (blocking) // always true for linux/mac
	{
		// Modal, so pause agent
		send_agent_pause();
	}


	success = doNavChooseDialog(filter);
		
	if (success)
	{
		if (!getFileCount())
			success = false;
	}

	if (blocking)
	{
		send_agent_resume();
		// Account for the fact that the app has been stalled.
		LLFrameTimer::updateFrameTime();
	}

	return success;
}

bool LLFilePicker::getMultipleOpenFiles(ELoadFilter filter, bool blocking)
{
	if( mLocked )
		return false;

	bool success = false;

	// if local file browsing is turned off, return without opening dialog
	if ( check_local_file_access_enabled() == false )
	{
		return false;
	}

	reset();
    
    mPickOptions |= F_FILE;

    mPickOptions |= F_MULTIPLE;

	if (blocking) // always true for linux/mac
	{
		// Modal, so pause agent
		send_agent_pause();
	}

	success = doNavChooseDialog(filter);

	if (blocking)
	{
		send_agent_resume();
	}

	if (success)
	{
		if (!getFileCount())
			success = false;
		if (getFileCount() > 1)
			mLocked = true;
	}

	// Account for the fact that the app has been stalled.
	LLFrameTimer::updateFrameTime();
	return success;
}

bool LLFilePicker::getSaveFile(ESaveFilter filter, const std::string& filename, bool blocking)
{

	if( mLocked )
		return false;
	bool success = false;

	// if local file browsing is turned off, return without opening dialog
	if ( check_local_file_access_enabled() == false )
	{
		return false;
	}

	reset();
	
    mPickOptions &= ~F_MULTIPLE;

	if (blocking)
	{
		// Modal, so pause agent
		send_agent_pause();
	}

    success = doNavSaveDialog(filter, filename);

    if (success)
	{
		if (!getFileCount())
			success = false;
	}

	if (blocking)
	{
		send_agent_resume();
	}

	// Account for the fact that the app has been stalled.
	LLFrameTimer::updateFrameTime();
	return success;
}


