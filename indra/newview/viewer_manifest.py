#!/usr/bin/env python
"""\
@file viewer_manifest.py
@author Ryan Williams
@brief Description of all installer viewer files, and methods for packaging
       them into installers for all supported platforms.

$LicenseInfo: firstyear=2006&license=viewerlgpl$
Second Life Viewer Source Code
Copyright (C) 2006-2014, Linden Research, Inc.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation;
version 2.1 of the License only.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
$/LicenseInfo$
"""
import errno
import json
import os
import os.path
import plistlib
import random
import re
import shutil
import stat
import subprocess
import sys
import tarfile
import time
import zipfile

viewer_dir = os.path.dirname(__file__)
global home_path
from os.path import expanduser
home_path = expanduser("~")
# Add indra/lib/python to our path so we don't have to muck with PYTHONPATH.
# Put it FIRST because some of our build hosts have an ancient install of
# indra.util.llmanifest under their system Python!
sys.path.insert(0, os.path.join(viewer_dir, os.pardir, "lib", "python"))
from indra.util.llmanifest import LLManifest, main, path_ancestors, CHANNEL_VENDOR_BASE, RELEASE_CHANNEL, ManifestError, MissingError
try:
    from llbase import llsd
except ImportError:
    from indra.base import llsd

class ViewerManifest(LLManifest):
    def is_packaging_viewer(self):
        # Some commands, files will only be included
        # if we are packaging the viewer on windows.
        # This manifest is also used to copy
        # files during the build (see copy_w_viewer_manifest
        # and copy_l_viewer_manifest targets)
        return 'package' in self.args['actions']
    
    def construct(self):
        super(ViewerManifest, self).construct()
        self.path(src="../../scripts/messages/message_template.msg", dst="app_settings/message_template.msg")
        self.path(src="../../etc/message.xml", dst="app_settings/message.xml")

        if self.is_packaging_viewer():
            # SLVoice executable
            binpkgdir = os.path.join(self.args['build'], os.pardir, 'packages', 'bin', 'release')
            with self.prefix(src=binpkgdir,dst=""):
                self.path("SLVoice")
                    
            with self.prefix(src="app_settings"):
                self.exclude("logcontrol.xml")
                self.exclude("logcontrol-dev.xml")
                self.path("*.ini")
                self.path("*.xml")
                self.path("*.db2")

                # include the entire shaders directory recursively
                self.path("shaders")
                # include the extracted list of contributors
                contributions_path = "../../doc/contributions.txt"
                contributor_names = self.extract_names(contributions_path)
                self.put_in_file(contributor_names, "contributors.txt", src=contributions_path)

                # ... and the entire windlight directory
                self.path("windlight")

                # ... and the entire image filters directory
                self.path("filters")
            
                # ... and the included spell checking dictionaries
                pkgdir = os.path.join(self.args['build'], os.pardir, 'packages')
                with self.prefix(src=pkgdir,dst=""):
                    self.path("dictionaries")
                    self.path("ca-bundle.crt")

                # include the extracted packages information (see BuildPackagesInfo.cmake)
                self.path(src=os.path.join(self.args['build'],"packages-info.txt"), dst="packages-info.txt")
                # CHOP-955: If we have "sourceid" or "viewer_channel" in the
                # build process environment, generate it into
                # settings_install.xml.
                settings_template = dict(
                    sourceid=dict(Comment='Identify referring agency to Linden web servers',
                                  Persist=1,
                                  Type='String',
                                  Value=''),
                    CmdLineGridChoice=dict(Comment='Default grid',
                                  Persist=0,
                                  Type='String',
                                  Value=''),
                    CmdLineChannel=dict(Comment='Command line specified channel name',
                                        Persist=0,
                                        Type='String',
                                        Value=''))
                settings_install = {}
                sourceid = self.args.get('sourceid')
                if sourceid:
                    settings_install['sourceid'] = settings_template['sourceid'].copy()
                    settings_install['sourceid']['Value'] = sourceid
                    print "Set sourceid in settings_install.xml to '%s'" % sourceid

                if self.args.get('channel_suffix'):
                    settings_install['CmdLineChannel'] = settings_template['CmdLineChannel'].copy()
                    settings_install['CmdLineChannel']['Value'] = self.channel_with_pkg_suffix()
                    print "Set CmdLineChannel in settings_install.xml to '%s'" % self.channel_with_pkg_suffix()

                if self.args.get('grid'):
                    settings_install['CmdLineGridChoice'] = settings_template['CmdLineGridChoice'].copy()
                    settings_install['CmdLineGridChoice']['Value'] = self.grid()
                    print "Set CmdLineGridChoice in settings_install.xml to '%s'" % self.grid()

                # put_in_file(src=) need not be an actual pathname; it
                # only needs to be non-empty
                self.put_in_file(llsd.format_pretty_xml(settings_install),
                                 "settings_install.xml",
                                 src="environment")


            with self.prefix(src="character"):
                self.path("*.llm")
                self.path("*.xml")
                self.path("*.tga")

            # Include our fonts
            # with self.prefix(src="fonts"):
            #    self.path("*.ttf")
            #    self.path("*.txt")

            # skins
            with self.prefix(src="skins"):
                    # include the entire textures directory recursively
                    with self.prefix(src="*/textures"):
                            #self.path("*/*.jpg")
                            self.path("*/*.png")
                            self.path("*.tga")
                            self.path("*.j2c")
                            self.path("*.png")
                            self.path("textures.xml")
                    self.path("*/xui/*/*.xml")
                    self.path("*/xui/*/widgets/*.xml")
                    self.path("*/*.xml")

                    # Local HTML files (e.g. loading screen)
                    # The claim is that we never use local html files any
                    # longer. But rather than commenting out this block, let's
                    # rename every html subdirectory as html.old. That way, if
                    # we're wrong, a user actually does have the relevant
                    # files; s/he just needs to rename every html.old
                    # directory back to html to recover them.
                    with self.prefix(src="*/html", dst="*/html"):
                            self.path("*.png")
                            self.path("*/*/*.html")
                            #self.path("*/*/*.gif")


            # The summary.json file gets left in the build directory by newview/CMakeLists.txt.
            if not self.path2basename(os.pardir, "summary.json"):
                print "No summary.json file"

    def grid(self):
        return self.args['grid']

    def channel(self):
        return self.args['channel']

    def channel_with_pkg_suffix(self):
        fullchannel=self.channel()
        channel_suffix = self.args.get('channel_suffix')
        if channel_suffix:
            fullchannel+=' '+channel_suffix
        return fullchannel

    def channel_variant(self):
        global CHANNEL_VENDOR_BASE
        return self.channel().replace(CHANNEL_VENDOR_BASE, "").strip()

    def channel_type(self): # returns 'release', 'beta', 'project', or 'test'
        global CHANNEL_VENDOR_BASE
        channel_qualifier=self.channel().replace(CHANNEL_VENDOR_BASE, "").lower().strip()
        if channel_qualifier.startswith('release'):
            channel_type='release'
        elif channel_qualifier.startswith('relwithdebinfo'):
            channel_type='release'    
        elif channel_qualifier.startswith('beta'):
            channel_type='beta'
        elif channel_qualifier.startswith('project'):
            channel_type='project'
        else:
            channel_type='test'
        return channel_type

    def channel_variant_app_suffix(self):
        # get any part of the compiled channel name after the CHANNEL_VENDOR_BASE
        suffix=self.channel_variant()
        # by ancient convention, we don't use Release in the app name
        if self.channel_type() == 'release':
            suffix=suffix.replace('Release', '').strip()
        # for the base release viewer, suffix will now be null - for any other, append what remains
        if suffix:
            suffix = "_".join([''] + suffix.split())
        # the additional_packages mechanism adds more to the installer name (but not to the app name itself)
        # ''.split() produces empty list, so suffix only changes if
        # channel_suffix is non-empty
        suffix = "_".join([suffix] + self.args.get('channel_suffix', '').split())
        return suffix

    def installer_base_name(self):
        global CHANNEL_VENDOR_BASE
        # a standard map of strings for replacing in the templates
        substitution_strings = {
            'channel_vendor_base' : '_'.join(CHANNEL_VENDOR_BASE.split()),
            'channel_variant_underscores':self.channel_variant_app_suffix(),
            'version_underscores' : '_'.join(self.args['version']),
            'arch':self.args['arch']
            }
        return "%(channel_vendor_base)s%(channel_variant_underscores)s_%(version_underscores)s_%(arch)s" % substitution_strings

    def app_name(self):
        global CHANNEL_VENDOR_BASE
        channel_type=self.channel_type()
        if channel_type == 'release':
            app_suffix='Viewer'
        else:
            app_suffix=self.channel_variant()
        return CHANNEL_VENDOR_BASE + ' ' + app_suffix
    def app_name_oneword(self):
        return ''.join(self.app_name().split())
    
    def icon_path(self):
        return "icons/" + self.channel_type()

    def extract_names(self,src):
        try:
            contrib_file = open(src,'r')
        except IOError:
            print "Failed to open '%s'" % src
            raise
        lines = contrib_file.readlines()
        contrib_file.close()

        # All lines up to and including the first blank line are the file header; skip them
        lines.reverse() # so that pop will pull from first to last line
        while not re.match("\s*$", lines.pop()) :
            pass # do nothing

        # A line that starts with a non-whitespace character is a name; all others describe contributions, so collect the names
        names = []
        for line in lines :
            if re.match("\S", line) :
                names.append(line.rstrip())
        # It's not fair to always put the same people at the head of the list
        random.shuffle(names)
        return ', '.join(names)



class DarwinManifest(ViewerManifest):
    
    def is_packaging_viewer(self):
        # darwin requires full app bundle packaging even for debugging.
        return True

    def construct(self):
        # copy over the build result (this is a no-op if run within the xcode script)
        self.path(self.args['configuration'] + "/Dayturn.app", dst="")

        pkgdir = os.path.join(self.args['build'], os.pardir, 'packages')
        relpkgdir = os.path.join(pkgdir, "lib", "release")
        debpkgdir = os.path.join(pkgdir, "lib", "debug")
        relpkgbindir = os.path.join(pkgdir, "bin", "release")

        with self.prefix(src="", dst="Contents"):  # everything goes in Contents
            #self.path("Info.plist", dst="Info.plist")

            # copy additional libs in <bundle>/Contents/MacOS/
            self.path(os.path.join(relpkgdir, "libndofdev.dylib"), dst="Resources/libndofdev.dylib")
            self.path(os.path.join(relpkgdir, "libhunspell-1.3.0.dylib"), dst="Resources/libhunspell-1.3.0.dylib")

            # most everything goes in the Resources directory
            with self.prefix(src="", dst="Resources"):
                super(DarwinManifest, self).construct()

                with self.prefix("cursors_mac"):
                    self.path("*.tif")

                self.path("licenses-mac.txt", dst="licenses.txt")
                self.path("featuretable_mac.txt")
                self.path("cube.dae")

                icon_path = self.icon_path()
                with self.prefix(src=icon_path, dst="") :
                    self.path("dayturn_icon.icns")

                with self.prefix(src="Base.lproj/Dayturn.nib", dst="Base.lproj/Dayturn.nib"):
                    self.path("*.nib")
                
                # Translations
                self.path("en.lproj/Dayturn.strings")
                self.replace_in(src="en.lproj/InfoPlist.strings",
                                dst="en.lproj/InfoPlist.strings",
                                searchdict={'%%VERSION%%':'.'.join(self.args['version'])}
                                )
                self.path("de.lproj/Dayturn.strings")
                self.path("ja.lproj/Dayturn.strings")
                self.path("ko.lproj/Dayturn.strings")
                self.path("da.lproj/Dayturn.strings")
                self.path("es.lproj/Dayturn.strings")
                self.path("fr.lproj/Dayturn.strings")
                self.path("hu-HU.lproj/Dayturn.strings")
                self.path("it.lproj/Dayturn.strings")
                self.path("nl.lproj/Dayturn.strings")
                self.path("pl.lproj/Dayturn.strings")
                self.path("pt-PT.lproj/Dayturn.strings")
                self.path("ru.lproj/Dayturn.strings")
                self.path("tr.lproj/Dayturn.strings")
                self.path("en-GB.lproj/Dayturn.strings")
                self.path("zh-Hans.lproj/Dayturn.strings")

                def path_optional(src, dst):
                    """
                    For a number of our self.path() calls, not only do we want
                    to deal with the absence of src, we also want to remember
                    which were present. Return either an empty list (absent)
                    or a list containing dst (present). Concatenate these
                    return values to get a list of all libs that are present.
                    """
                    # This was simple before we started needing to pass
                    # wildcards. Fortunately, self.path() ends up appending a
                    # (source, dest) pair to self.file_list for every expanded
                    # file processed. Remember its size before the call.
                    oldlen = len(self.file_list)
                    try:
                        self.path(src, dst)
                        # The dest appended to self.file_list has been prepended
                        # with self.get_dst_prefix(). Strip it off again.
                        added = [os.path.relpath(d, self.get_dst_prefix())
                                 for s, d in self.file_list[oldlen:]]
                    except MissingError as err:
                        print >> sys.stderr, "Warning: "+err.msg
                        added = []                    
                    if not added:
                        print "Skipping %s" % dst
                    return added

                # dylibs is a list of all the .dylib files we expect to need
                # in our bundled sub-apps. For each of these we'll create a
                # symlink from sub-app/Contents/Resources to the real .dylib.
                # Need to get the llcommon dll from any of the build directories as well.
                libfile_parent = self.get_dst_prefix()
                dylibs=[]
                for libfile in (
                                "libapr-1.0.dylib",
                                "libaprutil-1.0.dylib",
                                "libexpat.1.dylib",
                                # libnghttp2.dylib is a symlink to
                                # libnghttp2.major.dylib, which is a symlink to
                                # libnghttp2.version.dylib. Get all of them.                                                               
                                "libnghttp2.*dylib",         
                                "liburiparser.*dylib",
                                ):
                    dylibs += path_optional(os.path.join(relpkgdir, libfile), libfile)

                # Vivox libraries
                for libfile in (
                                'libortp.dylib',
                                'libvivoxsdk.dylib',
                                ):
                     self.path2basename(relpkgdir, libfile)

                # dylibs that vary based on configuration
                if self.args['configuration'].lower() == 'debug':
                    for libfile in (
                                "libfmodL.dylib",
                                ):
                        dylibs += path_optional(os.path.join(debpkgdir, libfile), libfile)
                else:
                    for libfile in (
                                "libfmod.dylib",
                                ):
                        dylibs += path_optional(os.path.join("../packages/lib/release",
                                                         libfile), libfile)
                
                # our apps
                executable_path = {}
                embedded_apps = [ (os.path.join("llplugin", "slplugin"), "SLPlugin.app") ]
                for app_bld_dir, app in embedded_apps:
                    self.path2basename(os.path.join(os.pardir,
                                                    app_bld_dir, self.args['configuration']),
                                       app)

                    # our apps dependencies on shared libs
                    # for each app, for each dylib we collected in dylibs,
                    # create a symlink to the real copy of the dylib.
                    resource_path = self.dst_path_of(os.path.join(app, "Contents", "Resources"))
                    for libfile in dylibs:
                        src = os.path.join(os.pardir, os.pardir, os.pardir, libfile)
                        dst = os.path.join(resource_path, libfile)
                        try:
                            symlinkf(src, dst)
                        except OSError as err:
                            print "Can't symlink %s -> %s: %s" % (src, dst, err)

                # Dullahan helper apps go inside SLPlugin.app
                if self.prefix(src="", dst="SLPlugin.app/Contents/Frameworks"):
                    helperappfile = 'DullahanHelper.app'
                    self.path2basename(relpkgdir, helperappfile)

                    pluginframeworkpath = self.dst_path_of('Chromium Embedded Framework.framework');
                    # Putting a Frameworks directory under Contents/MacOS
                    # isn't canonical, but the path baked into LLCefLib
                    # Helper.app/Contents/MacOS/LLCefLib Helper is:
                    # @executable_path/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework
                    # (notice, not @executable_path/../Frameworks/etc.)
                    # So we'll create a symlink (below) from there back to the
                    # Frameworks directory nested under SLPlugin.app.
                    helperframeworkpath = \
                        self.dst_path_of('DullahanHelper.app/Contents/MacOS/'
                                         'Frameworks/Chromium Embedded Framework.framework')
                    self.end_prefix()

                    helperexecutablepath = self.dst_path_of('SLPlugin.app/Contents/Frameworks/DullahanHelper.app/Contents/MacOS/DullahanHelper')
                    self.run_command('install_name_tool -change '
                                     '"@rpath/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework" '
                                     '"@executable_path/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework" "%s"' % helperexecutablepath)

                # SLPlugin plugins
                if self.prefix(src="", dst="llplugin"):
                    self.path2basename("../media_plugins/cef/" + self.args['configuration'],
                                       "media_plugin_cef.dylib")
                                       
                    # copy LibVLC plugin itself
                    self.path2basename("../media_plugins/libvlc/" + self.args['configuration'],
                                       "media_plugin_libvlc.dylib")

                    # copy LibVLC dynamic libraries
                    if self.prefix(src=os.path.join(os.pardir, 'packages', 'lib', 'release' ), dst="lib"):
                        self.path( "libvlc*.dylib*" )
                        self.end_prefix()

                    # copy LibVLC plugins folder
                    if self.prefix(src=os.path.join(os.pardir, 'packages', 'lib', 'release', 'plugins' ), dst="lib"):
                        self.path( "*.dylib" )
                        self.path( "plugins.dat" )
                        self.end_prefix()                                       
                    
                    self.end_prefix("llplugin")
                                       
                    # do this install_name_tool *after* media plugin is copied over
                    dylibexecutablepath = self.dst_path_of('llplugin/media_plugin_cef.dylib')
                    self.run_command('install_name_tool -change '
                                     '"@rpath/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework" '
                                     '"@executable_path/../Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework" "%s"' % dylibexecutablepath)

                    
                # Remove garbage in the Framework we get from LL 
                if self.prefix(src="", dst="Frameworks"):
                    #check if folder exists
                    if os.path.exists("SLPlugin Helper.app"):
                        # remove if exists
                        shutil.rmtree("SLPlugin Helper.app")
                self.end_prefix("Frameworks")
                
                
                # This code constructs a relative path from the
                # target framework folder back to the location of the symlink.
                # It needs to be relative so that the symlink still works when
                # (as is normal) the user moves the app bundle out of the DMG
                # and into the /Applications folder. Note we also call 'raise'
                # to terminate the process if we get an error since without
                # this symlink, Second Life web media can't possibly work.
                # Real Frameworks folder, with the symlink inside the bundled SLPlugin.app (and why it's relative)
                #   <top level>.app/Contents/Frameworks/Chromium Embedded Framework.framework/
                #   <top level>.app/Contents/Resources/SLPlugin.app/Contents/Frameworks/Chromium Embedded Framework.framework ->
                # It might seem simpler just to create a symlink Frameworks to
                # the parent of Chromimum Embedded Framework.framework. But
                # that would create a symlink cycle, which breaks our
                # packaging step. So make a symlink from Chromium Embedded
                # Framework.framework to the directory of the same name, which
                # is NOT an ancestor of the symlink.
                frameworkpath = os.path.join(os.pardir, os.pardir, os.pardir,
                                             "Frameworks",
                                             "Chromium Embedded Framework.framework")
                try:
                    # from SLPlugin.app/Contents/Frameworks/Chromium Embedded
                    # Framework.framework back to Second
                    # Life.app/Contents/Frameworks/Chromium Embedded Framework.framework
                    origin, target = pluginframeworkpath, os.path.join(os.pardir, frameworkpath)
                    symlinkf(target, origin)
                    # from SLPlugin.app/Contents/Frameworks/LLCefLib
                    # Helper.app/Contents/MacOS/Frameworks/Chromium Embedded
                    # Framework.framework back to
                    # SLPlugin.app/Contents/Frameworks/Chromium Embedded Framework.framework
                    self.cmakedirs(os.path.dirname(helperframeworkpath))
                    origin = helperframeworkpath
                    target = os.path.join(os.pardir, os.pardir, os.pardir, os.pardir, os.pardir, frameworkpath)
                    symlinkf(target, origin)
                except OSError as err:
                    print "Can't symlink %s -> %s: %s" % (origin, target, err)
                    raise


        # fix up media_plugin.dylib so it knows where to look for CEF files it needs
        self.run_command('install_name_tool -change "@executable_path/Chromium Embedded Framework" "@executable_path/../Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework" "%(config)s/Dayturn.app/Contents/Resources/llplugin/media_plugin_cef.dylib"' %
                        { 'config' : self.args['configuration'] })

        # NOTE: the -S argument to strip causes it to keep enough info for
        # annotated backtraces (i.e. function names in the crash log).  'strip' with no
        # arguments yields a slightly smaller binary but makes crash logs mostly useless.
        # This may be desirable for the final release.  Or not.
        if ("package" in self.args['actions'] or 
            "unpacked" in self.args['actions']):
            self.run_command('strip -S %(viewer_binary)r' %
                             { 'viewer_binary' : self.dst_path_of('Contents/MacOS/Dayturn')})

    def package_finish(self):
        global CHANNEL_VENDOR_BASE
        # MBW -- If the mounted volume name changes, it breaks the .DS_Store's background image and icon positioning.
        #  If we really need differently named volumes, we'll need to create multiple DS_Store file images, or use some other trick.

        volname=CHANNEL_VENDOR_BASE+" Installer"  # DO NOT CHANGE without understanding comment above

        imagename = self.installer_base_name()

        sparsename = imagename + ".sparseimage"
        finalname = imagename + ".dmg"
        # make sure we don't have stale files laying about
        self.remove(sparsename, finalname)

        self.run_command('hdiutil create %(sparse)r -volname %(vol)r -fs HFS+ -type SPARSE -megabytes 1300 -layout SPUD' % {
                'sparse':sparsename,
                'vol':volname})

        # mount the image and get the name of the mount point and device node
        try:
            hdi_output = subprocess.check_output(['hdiutil', 'attach', '-private', sparsename])
        except subprocess.CalledProcessError as err:
            sys.exit("failed to mount image at '%s'" % sparsename)
            
        try:
            devfile = re.search("/dev/disk([0-9]+)[^s]", hdi_output).group(0).strip()
            volpath = re.search('HFS\s+(.+)', hdi_output).group(1).strip()

            # Copy everything in to the mounted .dmg

            app_name = self.app_name()

            # Hack:
            # Because there is no easy way to coerce the Finder into positioning
            # the app bundle in the same place with different app names, we are
            # adding multiple .DS_Store files to svn. There is one for release,
            # one for release candidate and one for first look. Any other channels
            # will use the release .DS_Store, and will look broken.
            # - Ambroff 2008-08-20
            dmg_template = os.path.join(
                'installers', 'darwin', '%s-dmg' % self.channel_type())

            if not os.path.exists (self.src_path_of(dmg_template)):
                dmg_template = os.path.join ('installers', 'darwin', 'release-dmg')

            for s,d in {self.get_dst_prefix():app_name + ".app",
                        os.path.join(dmg_template, "_VolumeIcon.icns"): ".VolumeIcon.icns",
                        os.path.join(dmg_template, "background.jpg"): "background.jpg",
                        os.path.join(dmg_template, "_DS_Store"): ".DS_Store"}.items():
                print "Copying to dmg", s, d
                self.copy_action(self.src_path_of(s), os.path.join(volpath, d))

            # Hide the background image, DS_Store file, and volume icon file (set their "visible" bit)
            for f in ".VolumeIcon.icns", "background.jpg", ".DS_Store":
                pathname = os.path.join(volpath, f)
                self.run_command('SetFile -a V %r' % pathname)

            # Create the alias file (which is a resource file) from the .r
            self.run_command('Rez %r -o %r' %
                             (self.src_path_of("installers/darwin/release-dmg/Applications-alias.r"),
                              os.path.join(volpath, "Applications")))

            # Set the alias file's alias bit
            self.run_command('SetFile -a A %r' % os.path.join(volpath, "Applications"))

            # Set the disk image root's custom icon bit
            self.run_command('SetFile -a C %r' % volpath)

            # Sign the app if requested; 
            # do this in the copy that's in the .dmg so that the extended attributes used by 
            # the signature are preserved; moving the files using python will leave them behind
            # and invalidate the signatures.
            
            # Note: As of macOS Sierra, keychains are created with names postfixed with '-db' so for example, the
            #       SL Viewer keychain would by default be found in ~/Library/Keychains/viewer.keychain-db instead of
            #       just ~/Library/Keychains/viewer.keychain in earlier versions.
            #
            #       Because we have old OS files from previous versions of macOS on the build hosts, the configurations
            #       are different on each host. Some have viewer.keychain, some have viewer.keychain-db and some have both.
            #       As you can see in the line below, this script expects the Linden Developer cert/keys to be in viewer.keychain.
            #
            #       To correctly sign builds you need to make sure ~/Library/Keychains/viewer.keychain exists on the host
            #       and that it contains the correct cert/key. If a build host is set up with a clean version of macOS Sierra (or later)
            #       then you will need to change this line (and the one for 'codesign' command below) to point to right place or else
            #       pull in the cert/key into the default viewer keychain 'viewer.keychain-db' and export it to 'viewer.keychain'
            
            if 'signature' in self.args:
                #check if folder exists
                frameworks=os.path.join(volpath,self.app_name()+".app/Contents/Frameworks/")
                if os.path.exists(frameworks + "SLPlugin Helper.app"):
                    # remove if exists
                    shutil.rmtree(frameworks + "SLPlugin Helper.app")
                ad_hoc_res=os.path.join(volpath,self.app_name()+".app/Contents/Resources/")
                print "Attempting to sign dylibs in '%s'" % ad_hoc_res
                app_in_dmg=os.path.join(volpath,self.app_name()+".app")
                print "Attempting to sign '%s'" % app_in_dmg
                slplugin_in_dmg=os.path.join(volpath,self.app_name()+".app/Contents/Resources/SLPlugin.app")
                print "Attempting to sign '%s'" % slplugin_in_dmg
                identity = self.args['signature']
                if identity == '':
                    identity = 'Developer ID Application'
                viewer_keychain = "%(home_path)s/Library/Keychains/login.keychain-db"

                if 'VIEWER_SIGNING_PWD' in os.environ:
                    self.run_command('security unlock-keychain -p "%s" "%s/Library/Keychains/login.keychain-db"' % (os.environ['VIEWER_SIGNING_PWD'], home_path ) )
                    signed=False
                    sign_attempts=3
                    sign_retry_wait=15
                    while (not signed) and (sign_attempts > 0):
                        try:
                            sign_attempts-=1;
                            self.run_command(
                                # sign dylibs in Chromium Embedded Framework with my signature
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/Libraries/"+"libEGL.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/Libraries/"+"libGLESv2.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/Libraries/"+"libswiftshader_libEGL.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/Libraries/"+"libswiftshader_libGLESv2.dylib"
                                    })
                            self.run_command(
                                # Signing some fck-ed up Breakpad app LL has stuck inside resources of the Chromium Embedded Framework
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/Resources/crash_report_sender.app"
                                    })
                            self.run_command(
                                # re-sign Chromium Embedded Framework with my signature
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': frameworks + "Chromium Embedded Framework.framework/"+"Chromium Embedded Framework"
                                    })
                            self.run_command(
                                # sign dylibs in Resources with my signature
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libndofdev.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libapr-1.0.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libaprutil-1.0.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libexpat.1.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libfmod.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libhunspell-1.3.0.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libnghttp2.14.14.0.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "liburiparser.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libortp.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "libvivoxsdk.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "llplugin/media_plugin_cef.dylib"
                                    })
                            self.run_command(
                                'codesign --verbose --force --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "llplugin/media_plugin_libvlc.dylib"
                                    })
                            # sign SLVoice with my signature and enable hardened runtime on it
                            self.run_command(
                                'codesign --verbose --force --options runtime --keychain %(viewer_keychain)r --sign %(identity)r %(dylib)r' % {
                                    'viewer_keychain': viewer_keychain,
                                    'identity': identity,
                                    'dylib': ad_hoc_res + "SLVoice"
                                    })
                            # sign slplugin with my signature
                            self.run_command(
                                'codesign --verbose --deep --force --options runtime --strict=symlinks --entitlements "%(home_path)s/Developer/dayturn-viewer/slplugin.entitlements" --keychain "%(home_path)s/Library/Keychains/login.keychain" --sign %(identity)r %(bundle)r' % {
                                    'home_path' : home_path,
                                    'identity': identity,
                                    'bundle': slplugin_in_dmg
                                    })
                            # finally sign the app bundle with my signature
                            # for notarization add --options runtime 
                            self.run_command(
                                'codesign --verbose --deep --force --options runtime --strict --entitlements "%(home_path)s/Developer/dayturn-viewer/viewer.entitlements" --keychain "%(home_path)s/Library/Keychains/login.keychain" --sign %(identity)r %(bundle)r' % {
                                    'home_path' : home_path,
                                    'identity': identity,
                                    'bundle': app_in_dmg
                                    })
                            signed=True # if no exception was raised, the codesign worked
                        except ManifestError as err:
                            if sign_attempts:
                                print >> sys.stderr, "codesign failed, waiting %d seconds before retrying" % sign_retry_wait
                                time.sleep(sign_retry_wait)
                                sign_retry_wait*=2
                            else:
                                print >> sys.stderr, "Maximum codesign attempts exceeded; giving up"
                                raise

                    print 72*'='
                    import stat
                    print app_in_dmg
                    # Dayturn Viewer.app
                    for sub0 in os.listdir(app_in_dmg):
                        print '--{}'.format(sub0)
                        path0 = os.path.join(app_in_dmg, sub0)
                        if os.path.isfile(path0):
                            # shouldn't be any file here
                            with open(path0) as inf:
                                for line in inf:
                                    print '  {}'.format(line.rstrip())
                        elif os.path.isdir(path0):
                            # Contents
                            for sub1 in os.listdir(path0):
                                print '----{}'.format(sub1)
                                path1 = os.path.join(path0, sub1)
                                if os.path.isfile(path1):
                                    # Info.plist, PkgInfo
                                    with open(path1) as inf:
                                        for line in inf:
                                            print '    {}'.format(line.rstrip())
                                elif os.path.isdir(path1):
                                    # Frameworks, MacOS, Resources
                                    for sub2 in os.listdir(path1):
                                        path2 = os.path.join(path1, sub2)
                                        print '    {:04o} {}'.format(stat.S_IMODE(os.stat(path2).st_mode), sub2)
                    print 72*'='

        finally:
            # Unmount the image even if exceptions from any of the above 
            self.run_command('hdiutil detach -force %r' % devfile)

        print "Converting temp disk image to final disk image"
        self.run_command('hdiutil convert %(sparse)r -format UDZO -imagekey zlib-level=9 -o %(final)r' % {'sparse':sparsename, 'final':finalname})
        # get rid of the temp file
        self.package_file = finalname
        self.remove(sparsename)
        if 'signature' in self.args:
            self.run_command(
                'codesign --verbose --options runtime --timestamp --keychain %(viewer_keychain)r --sign %(identity)r %(final)r' % {
                    'viewer_keychain': viewer_keychain,
                    'identity': identity,
                    'final': finalname
                    })


class Darwin_x86_64_Manifest(DarwinManifest):
    pass



################################################################

def symlinkf(src, dst):
    """
    Like ln -sf, but uses os.symlink() instead of running ln.
    """
    try:
        os.symlink(src, dst)
    except OSError as err:
        if err.errno != errno.EEXIST:
            raise
        # We could just blithely attempt to remove and recreate the target
        # file, but that strategy doesn't work so well if we don't have
        # permissions to remove it. Check to see if it's already the
        # symlink we want, which is the usual reason for EEXIST.
        elif os.path.islink(dst):
            if os.readlink(dst) == src:
                # the requested link already exists
                pass
            else:
                # dst is the wrong symlink; attempt to remove and recreate it
                os.remove(dst)
                os.symlink(src, dst)
        elif os.path.isdir(dst):
            print "Requested symlink (%s) exists but is a directory; replacing" % dst
            shutil.rmtree(dst)
            os.symlink(src, dst)
        elif os.path.exists(dst):
            print "Requested symlink (%s) exists but is a file; replacing" % dst
            os.remove(dst)
            os.symlink(src, dst)
        else:
            # see if the problem is that the parent directory does not exist
            # and try to explain what is missing
            (parent, tail) = os.path.split(dst)
            while not os.path.exists(parent):
                (parent, tail) = os.path.split(parent)
            if tail:
                raise Exception("Requested symlink (%s) cannot be created because %s does not exist"
                                % os.path.join(parent, tail))
            else:
                raise

if __name__ == "__main__":
    # Report our own command line so that, in case of trouble, a developer can
    # manually rerun the same command.
    print('%s \\\n%s' %
          (sys.executable,
           ' '.join((("'%s'" % arg) if ' ' in arg else arg) for arg in sys.argv)))
    extra_arguments = [
        ]
    main(extra=extra_arguments)    
    try:
        main()
    except (ManifestError, MissingError) as err:
        sys.exit("\nviewer_manifest.py failed: "+err.msg)
    except:
        raise
