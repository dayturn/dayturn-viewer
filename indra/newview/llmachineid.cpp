/** 
 * @file llmachineid.cpp
 * @brief retrieves unique machine ids
 *
 * $LicenseInfo:firstyear=2009&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"
#include "llmachineid.h"
#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>
unsigned char static_unique_id[] =  {0,0,0,0,0,0};
unsigned char static_legacy_id[] =  {0,0,0,0,0,0};
bool static has_static_unique_id = false;
bool static has_static_legacy_id = false;

bool getSerialNumber(unsigned char *unique_id, size_t len)
{
    CFStringRef serial_cf_str = NULL;
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,
                                                                 IOServiceMatching("IOPlatformExpertDevice"));
    if (platformExpert)
    {
        serial_cf_str = (CFStringRef) IORegistryEntryCreateCFProperty(platformExpert,
                                                                     CFSTR(kIOPlatformSerialNumberKey),
                                                                     kCFAllocatorDefault, 0);
        IOObjectRelease(platformExpert);
    }
    
    if (serial_cf_str)
    {
        char buffer[64] = {0};
        std::string serial_str("");
        if (CFStringGetCString(serial_cf_str, buffer, 64, kCFStringEncodingUTF8))
        {
            serial_str = buffer;
        }

        S32 serial_size = serial_str.size();
        
        if(serial_str.size() > 0)
        {
            S32 j = 0;
            while (j < serial_size)
            {
                for (S32 i = 0; i < len; i++)
                {
                    if (j >= serial_size)
                        break;

                    unique_id[i] = (unsigned int)(unique_id[i] + serial_str[j]);
                    j++;
                }
            }
            return true;
        }
    }
    return false;
}

// get an unique machine id.
// NOT THREAD SAFE - do before setting up threads.
// MAC Address doesn't work for Windows 7 since the first returned hardware MAC address changes with each reboot,  Go figure??

S32 LLMachineID::init()
{
    size_t len = sizeof(static_unique_id);
    memset(static_unique_id, 0, len);
    S32 ret_code = 0;

    if (getSerialNumber(static_unique_id, len))
    {	
		has_static_unique_id = true;
        LL_DEBUGS("AppInit") << "Using Serial number as unique id" << LL_ENDL;
    }

    {
        unsigned char * staticPtr = (unsigned char *)(&static_legacy_id[0]);
        ret_code = LLUUID::getNodeID(staticPtr);
        has_static_legacy_id = true;
    }

    // Fallback to legacy
    if (!has_static_unique_id)
    {
        if (has_static_legacy_id)
        {
            memcpy(static_unique_id, &static_legacy_id, len);
            // Since ids are identical, mark legacy as not present
            // to not cause retry's in sechandler
            has_static_legacy_id = false;
            has_static_unique_id = true;
            LL_DEBUGS("AppInit") << "Using legacy serial" << LL_ENDL;
        }
    }
	
	LL_INFOS("AppInit") << "UniqueID: 0x";
	// Code between here and LL_ENDL is not executed unless the LL_DEBUGS
	// actually produces output
	for (size_t i = 0; i < len; ++i)
	{
		// Copy each char to unsigned int to hexify. Sending an unsigned
		// char to a std::ostream tries to represent it as a char, not
		// what we want here.
		unsigned byte = static_unique_id[i];
		LL_CONT << std::hex << std::setw(2) << std::setfill('0') << byte;
	}
	// Reset default output formatting to avoid nasty surprises!
	LL_CONT << std::dec << std::setw(0) << std::setfill(' ') << LL_ENDL;	
	
	return ret_code;
}



S32 LLMachineID::getUniqueID(unsigned char *unique_id, size_t len)
{
    if (has_static_unique_id)
    {
        memcpy ( unique_id, &static_unique_id, len);
        return 1;
    }
    return 0;
}


S32 LLMachineID::getLegacyID(unsigned char *unique_id, size_t len)
{
    if (has_static_legacy_id)
    {
        memcpy(unique_id, &static_legacy_id, len);
        return 1;
    }
    return 0;
}

