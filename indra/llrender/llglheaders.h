/**
 * @file llglheaders.h
 * @brief LLGL definitions
 *
 * $LicenseInfo:firstyear=2001&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLGLHEADERS_H
#define LL_LLGLHEADERS_H


//----------------------------------------------------------------------------
// LL_DARWIN

// NOTE This will SUPPRESS errors if using removed OpenGL functionality
#define GL_DO_NOT_WARN_IF_MULTI_GL_VERSION_HEADERS_INCLUDED

#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
//#import <OpenGL/gl3.h>
#import <OpenGL/glext.h>
#import <OpenGL/OpenGLAvailability.h>
#include <OpenGL/gltypes.h>


#include "GL/glh_extensions.h"


// GL_ARB_vertex_array_object Apple equivalent
#define glBindVertexArray glBindVertexArrayAPPLE
#define glGenVertexArrays glGenVertexArraysAPPLE
#define glDeleteVertexArrays glDeleteVertexArraysAPPLE
#define glIsVertexArray glIsVertexArrayAPPLE


// #define GL_EXT_separate_specular_color 1 // defined in OpenGL/glext.h

//
// Define map buffer range headers on Mac
//
// These are defined in <OpenGL/gl3.h> and currently unused by the viewer
/* 
#ifndef GL_ARB_map_buffer_range
#define GL_MAP_READ_BIT                   0x0001
#define GL_MAP_WRITE_BIT                  0x0002
#define GL_MAP_INVALIDATE_RANGE_BIT       0x0004
#define GL_MAP_INVALIDATE_BUFFER_BIT      0x0008
#define GL_MAP_FLUSH_EXPLICIT_BIT         0x0010
#define GL_MAP_UNSYNCHRONIZED_BIT         0x0020
#endif
 */

#ifdef __cplusplus
extern "C" {
#endif
//
// Define multisample headers on Mac
//
// These are defined in <OpenGL/gl3.h>

#ifndef GL_ARB_texture_multisample
#define GL_SAMPLE_POSITION                0x8E50
#define GL_SAMPLE_MASK                    0x8E51
#define GL_SAMPLE_MASK_VALUE              0x8E52
#define GL_MAX_SAMPLE_MASK_WORDS          0x8E59
#define GL_TEXTURE_2D_MULTISAMPLE         0x9100
#define GL_PROXY_TEXTURE_2D_MULTISAMPLE   0x9101
#define GL_TEXTURE_2D_MULTISAMPLE_ARRAY   0x9102
#define GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY 0x9103
#define GL_TEXTURE_BINDING_2D_MULTISAMPLE 0x9104
#define GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY 0x9105
#define GL_TEXTURE_SAMPLES                0x9106
#define GL_TEXTURE_FIXED_SAMPLE_LOCATIONS 0x9107
#define GL_SAMPLER_2D_MULTISAMPLE         0x9108
#define GL_INT_SAMPLER_2D_MULTISAMPLE     0x9109
#define GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE 0x910A
#define GL_SAMPLER_2D_MULTISAMPLE_ARRAY   0x910B
#define GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 0x910C
#define GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 0x910D
#define GL_MAX_COLOR_TEXTURE_SAMPLES      0x910E
#define GL_MAX_DEPTH_TEXTURE_SAMPLES      0x910F
#define GL_MAX_INTEGER_SAMPLES            0x9110
#endif

#ifdef __cplusplus
}
#endif

//
// Define vertex buffer object headers on Mac
//
// These are defined in <OpenGL/glext.h> included above
/*
#ifndef GL_ARB_vertex_buffer_object
#define GL_BUFFER_SIZE_ARB                0x8764
#define GL_BUFFER_USAGE_ARB               0x8765
#define GL_ARRAY_BUFFER_ARB               0x8892
#define GL_ELEMENT_ARRAY_BUFFER_ARB       0x8893
#define GL_ARRAY_BUFFER_BINDING_ARB       0x8894
#define GL_ELEMENT_ARRAY_BUFFER_BINDING_ARB 0x8895
#define GL_VERTEX_ARRAY_BUFFER_BINDING_ARB 0x8896
#define GL_NORMAL_ARRAY_BUFFER_BINDING_ARB 0x8897
#define GL_COLOR_ARRAY_BUFFER_BINDING_ARB 0x8898
#define GL_INDEX_ARRAY_BUFFER_BINDING_ARB 0x8899
#define GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING_ARB 0x889A
#define GL_EDGE_FLAG_ARRAY_BUFFER_BINDING_ARB 0x889B
#define GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING_ARB 0x889C
#define GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING_ARB 0x889D
#define GL_WEIGHT_ARRAY_BUFFER_BINDING_ARB 0x889E
#define GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ARB 0x889F
#define GL_READ_ONLY_ARB                  0x88B8
#define GL_WRITE_ONLY_ARB                 0x88B9
#define GL_READ_WRITE_ARB                 0x88BA
#define GL_BUFFER_ACCESS_ARB              0x88BB
#define GL_BUFFER_MAPPED_ARB              0x88BC
#define GL_BUFFER_MAP_POINTER_ARB         0x88BD
#define GL_STREAM_DRAW_ARB                0x88E0
#define GL_STREAM_READ_ARB                0x88E1
#define GL_STREAM_COPY_ARB                0x88E2
#define GL_STATIC_DRAW_ARB                0x88E4
#define GL_STATIC_READ_ARB                0x88E5
#define GL_STATIC_COPY_ARB                0x88E6
#define GL_DYNAMIC_DRAW_ARB               0x88E8
#define GL_DYNAMIC_READ_ARB               0x88E9
#define GL_DYNAMIC_COPY_ARB               0x88EA
#endif
*/



//#ifndef GL_ARB_vertex_buffer_object
/* GL types for handling large vertex buffer objects */
/*
typedef intptr_t GLintptrARB;
typedef intptr_t GLsizeiptrARB;
// These are defined in <OpenGL/gltypes.h>
*/ 
//#endif


// These are defined in <OpenGL/glext.h> included above
// #ifndef GL_ARB_vertex_buffer_object
// #define GL_ARB_vertex_buffer_object 1
// #ifdef GL_GLEXT_FUNCTION_POINTERS
// typedef void (* glBindBufferARBProcPtr) (GLenum target, GLuint buffer);
// typedef void (* glDeleteBufferARBProcPtr) (GLsizei n, const GLuint *buffers);
// typedef void (* glGenBuffersARBProcPtr) (GLsizei n, GLuint *buffers);
// typedef GLboolean (* glIsBufferARBProcPtr) (GLuint buffer);
// typedef void (* glBufferDataARBProcPtr) (GLenum target, GLsizeiptrARB size, const GLvoid *data, GLenum usage);
// typedef void (* glBufferSubDataARBProcPtr) (GLenum target, GLintptrARB offset, GLsizeiptrARB size, const GLvoid *data);
// typedef void (* glGetBufferSubDataARBProcPtr) (GLenum target, GLintptrARB offset, GLsizeiptrARB size, GLvoid *data);
// typedef GLvoid* (* glMapBufferARBProcPtr) (GLenum target, GLenum access);	/* Flawfinder: ignore */
// typedef GLboolean (* glUnmapBufferARBProcPtr) (GLenum target);
// typedef void (* glGetBufferParameterivARBProcPtr) (GLenum target, GLenum pname, GLint *params);
// typedef void (* glGetBufferPointervARBProcPtr) (GLenum target, GLenum pname, GLvoid* *params);
// #else
// extern void glBindBufferARB (GLenum, GLuint);
// extern void glDeleteBuffersARB (GLsizei, const GLuint *);
// extern void glGenBuffersARB (GLsizei, GLuint *);
// extern GLboolean glIsBufferARB (GLuint);
// extern void glBufferDataARB (GLenum, GLsizeiptrARB, const GLvoid *, GLenum);
// extern void glBufferSubDataARB (GLenum, GLintptrARB, GLsizeiptrARB, const GLvoid *);
// extern void glGetBufferSubDataARB (GLenum, GLintptrARB, GLsizeiptrARB, GLvoid *);
// extern GLvoid* glMapBufferARB (GLenum, GLenum);
// extern GLboolean glUnmapBufferARB (GLenum);
// extern void glGetBufferParameterivARB (GLenum, GLenum, GLint *);
// extern void glGetBufferPointervARB (GLenum, GLenum, GLvoid* *);
// #endif /* GL_GLEXT_FUNCTION_POINTERS */
// #endif

// These are defined in <OpenGL/glext.h> included above
/* 
#ifndef GL_ARB_texture_rg
#define GL_RG                             0x8227
#define GL_RG_INTEGER                     0x8228
#define GL_R8                             0x8229
#define GL_R16                            0x822A
#define GL_RG8                            0x822B
#define GL_RG16                           0x822C
#define GL_R16F                           0x822D
#define GL_R32F                           0x822E
#define GL_RG16F                          0x822F
#define GL_RG32F                          0x8230
#define GL_R8I                            0x8231
#define GL_R8UI                           0x8232
#define GL_R16I                           0x8233
#define GL_R16UI                          0x8234
#define GL_R32I                           0x8235
#define GL_R32UI                          0x8236
#define GL_RG8I                           0x8237
#define GL_RG8UI                          0x8238
#define GL_RG16I                          0x8239
#define GL_RG16UI                         0x823A
#define GL_RG32I                          0x823B
#define GL_RG32UI                         0x823C
#endif
 */


// Even when GL_ARB_depth_clamp is available in the driver, the (correct)
// headers, and therefore GL_DEPTH_CLAMP might not be defined.
// In that case GL_DEPTH_CLAMP_NV should be defined, but why not just
// use the known numeric.
//
// To avoid #ifdef's in the code. Just define this here.
// These are defined in <OpenGL/glext.h> included above
//#ifndef GL_ARB_depth_clamp
// Probably (still) called GL_DEPTH_CLAMP_NV.
//#define GL_DEPTH_CLAMP 0x864F
//#endif

// Point Parameters on Mac
// These are defined in <OpenGL/glext.h> included above
/* 
#ifndef GL_ARB_point_parameters
#define GL_POINT_SIZE_MIN_ARB                                0x8126
#define GL_POINT_SIZE_MAX_ARB                                0x8127
#define GL_POINT_FADE_THRESHOLD_SIZE_ARB                     0x8128
#define GL_POINT_DISTANCE_ATTENUATION_ARB                    0x8129
#endif
 */

#endif // LL_LLGLHEADERS_H
