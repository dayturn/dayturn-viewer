# -*- cmake -*-
include(Prebuilt)

set(OpenSSL_FIND_QUIETLY ON)
set(OpenSSL_FIND_REQUIRED ON)

if (USESYSTEMLIBS)
  include(FindOpenSSL)
else (USESYSTEMLIBS)
  use_prebuilt_binary(openssl)
  set(OPENSSL_LIBRARIES ssl crypto)
  set(OPENSSL_INCLUDE_DIRS ${LIBS_PREBUILT_DIR}/include)
endif (USESYSTEMLIBS)

set(CRYPTO_LIBRARIES crypto)
