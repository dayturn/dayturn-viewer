# -*- cmake -*-

include(Variables)
include(FreeType)
include(GLH)

set(LLRENDER_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/llrender
    ${GLH_INCLUDE_DIR}
    )

set(LLRENDER_LIBRARIES
    llrender
    )

