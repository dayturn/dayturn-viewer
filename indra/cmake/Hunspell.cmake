# -*- cmake -*-
include(Prebuilt)

set(HUNSPELL_FIND_QUIETLY ON)
set(HUNSPELL_FIND_REQUIRED ON)

if (USESYSTEMLIBS)
  include(FindHUNSPELL)
else (USESYSTEMLIBS)
  use_prebuilt_binary(libhunspell)
  set(HUNSPELL_LIBRARY hunspell-1.3)
  set(HUNSPELL_INCLUDE_DIRS ${LIBS_PREBUILT_DIR}/include/hunspell)
  use_prebuilt_binary(dictionaries)
endif (USESYSTEMLIBS)
