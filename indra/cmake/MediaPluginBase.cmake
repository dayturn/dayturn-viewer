# -*- cmake -*-


set(Boost_FIND_QUIETLY ON)
set(Boost_FIND_REQUIRED ON)

if (USESYSTEMLIBS)
  include(FindBoost)

set(BOOST_FIBER_LIBRARY boost_fiber-mt)

endif (USESYSTEMLIBS)

set(MEDIA_PLUGIN_BASE_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/media_plugins/base/
    )

set(MEDIA_PLUGIN_BASE_LIBRARIES media_plugin_base)
