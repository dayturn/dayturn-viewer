# -*- cmake -*-

  set(OS_DRAG_DROP ON CACHE BOOL "Build the viewer with OS level drag and drop turned on or off")

  if (OS_DRAG_DROP)

  add_definitions(-DLL_OS_DRAGDROP_ENABLED=1)

  endif (OS_DRAG_DROP)

