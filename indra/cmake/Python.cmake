# -*- cmake -*-

set(PYTHONINTERP_FOUND)

  # On MAC OS X be sure to search standard locations first

  string(REPLACE ":" ";" PATH_LIST "$ENV{PATH}")
  find_program(PYTHON_EXECUTABLE
    NAMES python python27
    NO_DEFAULT_PATH # Avoid searching non-standard locations first
    PATHS
    /bin
    /usr/bin
    /usr/local/bin
    ${PATH_LIST}
    )

  if (PYTHON_EXECUTABLE)
    set(PYTHONINTERP_FOUND ON)
  endif (PYTHON_EXECUTABLE)
  include(FindPython)

if (NOT PYTHON_EXECUTABLE)
  message(FATAL_ERROR "No Python interpreter found")
endif (NOT PYTHON_EXECUTABLE)

mark_as_advanced(PYTHON_EXECUTABLE)
