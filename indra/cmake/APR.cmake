include(BerkeleyDB)
include(Linking)
include(Prebuilt)

set(APR_FIND_QUIETLY ON)
set(APR_FIND_REQUIRED ON)

set(APRUTIL_FIND_QUIETLY ON)
set(APRUTIL_FIND_REQUIRED ON)

if (USESYSTEMLIBS)
  include(FindAPR)
else (USESYSTEMLIBS)
  use_prebuilt_binary(apr_suite)
    if (LLCOMMON_LINK_SHARED)
      set(APR_selector     "0.dylib")
      set(APRUTIL_selector "0.dylib")
    else (LLCOMMON_LINK_SHARED)
      set(APR_selector     "a")
      set(APRUTIL_selector "a")
    endif (LLCOMMON_LINK_SHARED)
    set(APR_LIBRARIES libapr-1.${APR_selector})
    set(APRUTIL_LIBRARIES libaprutil-1.${APRUTIL_selector})
    set(APRICONV_LIBRARIES iconv)
  set(APR_INCLUDE_DIR ${LIBS_PREBUILT_DIR}/include/apr-1)

endif (USESYSTEMLIBS)

