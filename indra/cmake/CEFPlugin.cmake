# -*- cmake -*-

include(Linking)
include(Prebuilt)

if (USESYSTEMLIBS)
    set(CEFPLUGIN OFF CACHE BOOL
        "CEFPLUGIN support for the llplugin/llmedia test apps.")
else (USESYSTEMLIBS)
    use_prebuilt_binary(dullahan)
    set(CEFPLUGIN ON CACHE BOOL
        "CEFPLUGIN support for the llplugin/llmedia test apps.")
        set(CEF_INCLUDE_DIR ${LIBS_PREBUILT_DIR}/include/cef)
endif (USESYSTEMLIBS)


FIND_LIBRARY(APPKIT_LIBRARY AppKit)
if (NOT APPKIT_LIBRARY)
    message(FATAL_ERROR "AppKit not found")
endif()

FIND_LIBRARY(CEF_LIBRARY "Chromium Embedded Framework" ${ARCH_PREBUILT_DIRS_RELEASE})
if (NOT CEF_LIBRARY)
    message(FATAL_ERROR "CEF not found")
else (NOT CEF_LIBRARY)
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${ARCH_PREBUILT_DIRS_RELEASE}/Chromium\ Embedded\ Framework.framework ${CMAKE_SOURCE_DIR}/frameworks/Chromium\ Embedded\ Framework.framework)
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${ARCH_PREBUILT_DIRS_RELEASE}/DullahanHelper.app ${CMAKE_SOURCE_DIR}/frameworks/DullahanHelper.app)
endif()

set(CEF_PLUGIN_LIBRARIES
    ${ARCH_PREBUILT_DIRS_RELEASE}/libcef_dll_wrapper.a
    ${ARCH_PREBUILT_DIRS_RELEASE}/libdullahan.a
    ${APPKIT_LIBRARY}
    ${CEF_LIBRARY}
   )
   
