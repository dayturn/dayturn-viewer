# -*- cmake -*-

set(ZLIB_FIND_QUIETLY ON)
set(ZLIB_FIND_REQUIRED ON)

include(Prebuilt)

if (USESYSTEMLIBS)
  include(FindZLIB)
else (USESYSTEMLIBS)
  use_prebuilt_binary(zlib)
  set(ZLIB_LIBRARIES z)
  set(ZLIB_INCLUDE_DIRS ${LIBS_PREBUILT_DIR}/include/zlib)
endif (USESYSTEMLIBS)
