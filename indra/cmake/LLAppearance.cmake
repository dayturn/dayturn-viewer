# -*- cmake -*-

include(Variables)

set(LLAPPEARANCE_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/llappearance
    )

set(LLAPPEARANCE_LIBRARIES llappearance
    llmessage
    llcorehttp
    ${BOOST_FIBER_LIBRARY}
    ${BOOST_CONTEXT_LIBRARY}
    ${BOOST_SYSTEM_LIBRARY}
    )


