# -*- cmake -*-
include(Prebuilt)

set(PNG_FIND_QUIETLY ON)
set(PNG_FIND_REQUIRED ON)

if (USESYSTEMLIBS)
  include(FindPNG)
else (USESYSTEMLIBS)
  use_prebuilt_binary(libpng)
  set(PNG_LIBRARIES png16)
  set(PNG_INCLUDE_DIRS ${LIBS_PREBUILT_DIR}/include/libpng16)
endif (USESYSTEMLIBS)
