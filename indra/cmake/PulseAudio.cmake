# -*- cmake -*-
include(Prebuilt)

set(PULSEAUDIO ON CACHE BOOL "Build with PulseAudio support, if available.")

if (PULSEAUDIO)
  if (USESYSTEMLIBS)
    include(FindPkgConfig)

    pkg_check_modules(PULSEAUDIO libpulse)

  endif (USESYSTEMLIBS)
endif (PULSEAUDIO)

if (PULSEAUDIO_FOUND)
  add_definitions(-DLL_PULSEAUDIO_ENABLED=1)
endif (PULSEAUDIO_FOUND)
