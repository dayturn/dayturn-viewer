# -*- cmake -*-

# these should be moved to their own cmake file
include(Prebuilt)
include(Boost)

use_prebuilt_binary(colladadom)
use_prebuilt_binary(pcre)

find_package(LibXml2 REQUIRED)

if (LIBXML2_FOUND)
	set(LIBXML2_LIBRARIES /usr/lib/libxml2.dylib)
	MESSAGE(STATUS "Set libxml2: ${LIBXML2_LIBRARIES}")
	set(LIBXML2_INCLUDE_DIRS /usr/include)
	MESSAGE(STATUS "Set libxml2 Includes: ${LIBXML2_INCLUDE_DIRS}")
else (LIBXML2_FOUND)
	use_prebuilt_binary(libxml2)
endif (LIBXML2_FOUND)


set(LLPRIMITIVE_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/llprimitive
    )

set(LLPRIMITIVE_LIBRARIES 
    llprimitive
    debug collada14dom-d
    optimized collada14dom
    minizip
    xml2
    pcrecpp
    pcre
    iconv           # Required by libxml2
    )


