/** 
 * @file llwindowmacosx.h
 * @brief Mac implementation of LLWindow class
 *
 * $LicenseInfo:firstyear=2001&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLWINDOWMACOSX_H
#define LL_LLWINDOWMACOSX_H

#include "llwindow.h"
#include "llwindowcallbacks.h"
#include "llwindowmacosx-objc.h"

#include "lltimer.h"

#include <ApplicationServices/ApplicationServices.h>
#include <OpenGL/OpenGL.h>

// AssertMacros.h does bad things.
#include "fix_macros.h"
#undef verify
#undef require


class LLWindowMacOSX : public LLWindow
{
public:
	/*virtual*/ void show();
	/*virtual*/ void hide();
	/*virtual*/ void close();
	/*virtual*/ bool getVisible();
	/*virtual*/ bool getMinimized();
	/*virtual*/ bool getMaximized();
	/*virtual*/ bool maximize();
	/*virtual*/ void minimize();
	/*virtual*/ void restore();
	/*virtual*/ bool getFullscreen();
	/*virtual*/ bool getPosition(LLCoordScreen *position);
	/*virtual*/ bool getSize(LLCoordScreen *size);
	/*virtual*/ bool getSize(LLCoordWindow *size);
	/*virtual*/ bool setPosition(LLCoordScreen position);
	/*virtual*/ bool setSizeImpl(LLCoordScreen size);
	/*virtual*/ bool setSizeImpl(LLCoordWindow size);
	/*virtual*/ bool switchContext(bool fullscreen, const LLCoordScreen &size, bool disable_vsync, const LLCoordScreen * const posp = NULL);
	/*virtual*/ bool setCursorPosition(LLCoordWindow position);
	/*virtual*/ bool getCursorPosition(LLCoordWindow *position);
	/*virtual*/ void showCursor();
	/*virtual*/ void hideCursor();
	/*virtual*/ void showCursorFromMouseMove();
	/*virtual*/ void hideCursorUntilMouseMove();
	/*virtual*/ bool isCursorHidden();
	/*virtual*/ void updateCursor();
	/*virtual*/ ECursorType getCursor() const;
	/*virtual*/ void captureMouse();
	/*virtual*/ void releaseMouse();
	/*virtual*/ void setMouseClipping( bool b );
	/*virtual*/ bool isClipboardTextAvailable();
	/*virtual*/ bool pasteTextFromClipboard(LLWString &dst);
	/*virtual*/ bool copyTextToClipboard(const LLWString & src);
	/*virtual*/ void flashIcon(F32 seconds);
	/*virtual*/ F32 getGamma();
	/*virtual*/ bool setGamma(const F32 gamma); // Set the gamma
	/*virtual*/ U32 getFSAASamples();
	/*virtual*/ void setFSAASamples(const U32 fsaa_samples);
	/*virtual*/ bool restoreGamma();			// Restore original gamma table (before updating gamma)
	/*virtual*/ ESwapMethod getSwapMethod() { return mSwapMethod; }
	/*virtual*/ void gatherInput();
	/*virtual*/ void delayInputProcessing() {};
	/*virtual*/ void swapBuffers();
	
	// handy coordinate space conversion routines
	/*virtual*/ bool convertCoords(LLCoordScreen from, LLCoordWindow *to);
	/*virtual*/ bool convertCoords(LLCoordWindow from, LLCoordScreen *to);
	/*virtual*/ bool convertCoords(LLCoordWindow from, LLCoordGL *to);
	/*virtual*/ bool convertCoords(LLCoordGL from, LLCoordWindow *to);
	/*virtual*/ bool convertCoords(LLCoordScreen from, LLCoordGL *to);
	/*virtual*/ bool convertCoords(LLCoordGL from, LLCoordScreen *to);

	/*virtual*/ LLWindowResolution* getSupportedResolutions(S32 &num_resolutions);
	/*virtual*/ F32	getNativeAspectRatio();
	/*virtual*/ F32 getPixelAspectRatio();
	/*virtual*/ void setNativeAspectRatio(F32 ratio) { mOverrideAspectRatio = ratio; }

	/*virtual*/ void beforeDialog();
	/*virtual*/ void afterDialog();

	/*virtual*/ bool dialogColorPicker(F32 *r, F32 *g, F32 *b);

	/*virtual*/ void *getPlatformWindow();
	/*virtual*/ void bringToFront() {};
	
	/*virtual*/ void allowLanguageTextInput(LLPreeditor *preeditor, bool b);
	/*virtual*/ void interruptLanguageTextInput();
	/*virtual*/ void spawnWebBrowser(const std::string& escaped_url, bool async);
    /*virtual*/ F32 getSystemUISize();
	/*virtual*/ void openFile(const std::string& file_name);
	/*virtual*/ void setTitle(const std::string& title);

	static std::vector<std::string> getDisplaysResolutionList();

	static std::vector<std::string> getDynamicFallbackFontList();

	// Provide native key event data
	/*virtual*/ LLSD getNativeKeyData();
	
	void* getWindow() { return mWindow; }
	LLWindowCallbacks* getCallbacks() { return mCallbacks; }
	LLPreeditor* getPreeditor() { return mPreeditor; }
	
	void updateMouseDeltas(float* deltas);
	void getMouseDeltas(float* delta);
	
	void handleDragNDrop(std::string url, LLWindowCallbacks::DragNDropAction action);
    
    bool allowsLanguageInput() { return mLanguageTextInputAllowed; }

protected:
	LLWindowMacOSX(LLWindowCallbacks* callbacks,
		const std::string& title, const std::string& name, int x, int y, int width, int height, U32 flags,
		bool fullscreen, bool disable_vsync,
		bool ignore_pixel_depth,
		U32 fsaa_samples);
	~LLWindowMacOSX();

	void	initCursors();
	bool	isValid();
	void	moveWindow(const LLCoordScreen& position,const LLCoordScreen& size);


	// Changes display resolution. Returns true if successful
	bool	setDisplayResolution(S32 width, S32 height, S32 bits, S32 refresh);

	// Go back to last fullscreen display resolution.
	bool	setFullscreenResolution();

	// Restore the display resolution to its value before we ran the app.
	bool	resetDisplayResolution();

	bool	shouldPostQuit() { return mPostQuit; }
    
    //Satisfy MAINT-3135 and MAINT-3288 with a flag.
    /*virtual */ void setOldResize(bool oldresize) {setResizeMode(oldresize, mGLView); }

private:
    void restoreGLContext();

protected:
	//
	// Platform specific methods
	//

	// create or re-create the GL context/window.  Called from the constructor and switchContext().
	bool createContext(int x, int y, int width, int height, int bits, bool fullscreen, bool disable_vsync);
	void destroyContext();
	void setupFailure(const std::string& text, const std::string& caption, U32 type);
	void adjustCursorDecouple(bool warpingMouse = false);
	static MASK modifiersToMask(S16 modifiers);
	
#if LL_OS_DRAGDROP_ENABLED
	
	//static OSErr dragTrackingHandler(DragTrackingMessage message, WindowRef theWindow, void * handlerRefCon, DragRef theDrag);
	//static OSErr dragReceiveHandler(WindowRef theWindow, void * handlerRefCon,	DragRef theDrag);
	
	
#endif // LL_OS_DRAGDROP_ENABLED
	
	//
	// Platform specific variables
	//
	
	// Use generic pointers here.  This lets us do some funky Obj-C interop using Obj-C objects without having to worry about any compilation problems that may arise.
	NSWindowRef			mWindow;
	GLViewRef			mGLView;
	CGLContextObj		mContext;
	CGLPixelFormatObj	mPixelFormat;
	CGDirectDisplayID	mDisplay;
	
	LLRect		mOldMouseClip;  // Screen rect to which the mouse cursor was globally constrained before we changed it in clipMouse()
	std::string mWindowTitle;
	double		mOriginalAspectRatio;
	bool		mSimulatedRightClick;
	U32			mLastModifiers;
	bool		mHandsOffEvents;	// When true, temporarially disable CarbonEvent processing.
	// Used to allow event processing when putting up dialogs in fullscreen mode.
	bool		mCursorDecoupled;
	S32			mCursorLastEventDeltaX;
	S32			mCursorLastEventDeltaY;
	bool		mCursorIgnoreNextDelta;
	bool		mNeedsResize;		// Constructor figured out the window is too big, it needs a resize.
	LLCoordScreen   mNeedsResizeSize;
	F32			mOverrideAspectRatio;
	bool		mMaximized;
	bool		mMinimized;
	U32			mFSAASamples;
	bool		mForceRebuild;
	
	S32	mDragOverrideCursor;

	// Input method management through Text Service Manager.
	bool		mLanguageTextInputAllowed;
	LLPreeditor*	mPreeditor;
	
	friend class LLWindowManager;
	
};


class LLSplashScreenMacOSX : public LLSplashScreen
{
public:
	LLSplashScreenMacOSX();
	virtual ~LLSplashScreenMacOSX();

	/*virtual*/ void showImpl();
	/*virtual*/ void updateImpl(const std::string& mesg);
	/*virtual*/ void hideImpl();

private:
	WindowRef   mWindow;
};

S32 OSMessageBoxMacOSX(const std::string& text, const std::string& caption, U32 type);

void load_url_external(const char* url);

#endif //LL_LLWINDOWMACOSX_H
