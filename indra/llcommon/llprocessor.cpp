/** 
 * @file llprocessor.cpp
 * @brief Code to figure out the processor. Originally by Benjamin Jurke.
 *
 * $LicenseInfo:firstyear=2002&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "linden_common.h"
#include "llprocessor.h"
#include "llstring.h"
#include "stringize.h"
#include "llerror.h"

#include <iomanip>
//#include <memory>


#include "llsd.h"

#if LL_MSVC && _M_X64
#      define LL_X86_64 1
#      define LL_X86 1
#elif LL_MSVC && _M_IX86
#      define LL_X86 1
#elif LL_CLANG && ( defined(__amd64__) || defined(__x86_64__) )
#      define LL_X86_64 1
#      define LL_X86 1
#elif LL_CLANG && ( defined(__i386__) )
#      define LL_X86 1
#elif LL_GNUC && ( defined(__amd64__) || defined(__x86_64__) )
#      define LL_X86_64 1
#      define LL_X86 1
#elif LL_GNUC && ( defined(__i386__) )
#      define LL_X86 1
#elif LL_GNUC && ( defined(__powerpc__) || defined(__ppc__) )
#      define LL_PPC 1
#endif

class LLProcessorInfoImpl; // foward declaration for the mImpl;

namespace 
{
	enum cpu_info
	{
		eBrandName = 0,
		eFrequency,
		eVendor,
		eStepping,
		eFamily,
		eExtendedFamily,
		eModel,
		eExtendedModel,
		eType,
		eBrandID,
		eFamilyName
	};
		

	const char* cpu_info_names[] = 
	{
		"Processor Name",
		"Frequency",
		"Vendor",
		"Stepping",
		"Family",
		"Extended Family",
		"Model",
		"Extended Model",
		"Type",
		"Brand ID",
		"Family Name"
	};

	enum cpu_config
	{
		eMaxID,
		eMaxExtID,
		eCLFLUSHCacheLineSize,
		eAPICPhysicalID,
		eCacheLineSize,
		eL2Associativity,
		eCacheSizeK,
		eFeatureBits,
		eExtFeatureBits
	};

	const char* cpu_config_names[] =
	{
		"Max Supported CPUID level",
		"Max Supported Ext. CPUID level",
		"CLFLUSH cache line size",
		"APIC Physical ID",
		"Cache Line Size", 
		"L2 Associativity",
		"Cache Size",
		"Feature Bits",
		"Ext. Feature Bits"
	};



	// *NOTE:Mani - this contains the elements we reference directly and extensions beyond the first 32.
	// The rest of the names are referenced by bit maks returned from cpuid.
	enum cpu_features 
	{
		eSSE_Ext=25,
		eSSE2_Ext=26,

		eSSE3_Features=32,
		eMONTIOR_MWAIT=33,
		eCPLDebugStore=34,
		eThermalMonitor2=35,
	};

	const char* cpu_feature_names[] =
	{
		"x87 FPU On Chip",
		"Virtual-8086 Mode Enhancement",
		"Debugging Extensions",
		"Page Size Extensions",
		"Time Stamp Counter",
		"RDMSR and WRMSR Support",
		"Physical Address Extensions",
		"Machine Check Exception",
		"CMPXCHG8B Instruction",
		"APIC On Chip",
		"Unknown1",
		"SYSENTER and SYSEXIT",
		"Memory Type Range Registers",
		"PTE Global Bit",
		"Machine Check Architecture",
		"Conditional Move/Compare Instruction",
		"Page Attribute Table",
		"Page Size Extension",
		"Processor Serial Number",
		"CFLUSH Extension",
		"Unknown2",
		"Debug Store",
		"Thermal Monitor and Clock Ctrl",
		"MMX Technology",
		"FXSAVE/FXRSTOR",
		"SSE Extensions",
		"SSE2 Extensions",
		"Self Snoop",
		"Hyper-threading Technology",
		"Thermal Monitor",
		"Unknown4",
		"Pend. Brk. EN.", // 31 End of FeatureInfo bits

		"SSE3 New Instructions", // 32
		"MONITOR/MWAIT", 
		"CPL Qualified Debug Store",
		"Thermal Monitor 2",
	};

	std::string intel_CPUFamilyName(int composed_family) 
	{
		switch(composed_family)
		{
		case 3: return "Intel i386";
		case 4: return "Intel i486";
		case 5: return "Intel Pentium";
		case 6: return "Intel Pentium Pro/2/3, Core";
		case 7: return "Intel Itanium (IA-64)";
		case 0xF: return "Intel Pentium 4";
		case 0x10: return "Intel Itanium 2 (IA-64)";
		}
		return STRINGIZE("Intel <unknown 0x" << std::hex << composed_family << ">");
	}
	
	std::string amd_CPUFamilyName(int composed_family) 
	{
        // https://en.wikipedia.org/wiki/List_of_AMD_CPU_microarchitectures
        // https://developer.amd.com/resources/developer-guides-manuals/
		switch(composed_family)
		{
		case 4: return "AMD 80486/5x86";
		case 5: return "AMD K5/K6";
		case 6: return "AMD K7";
		case 0xF: return "AMD K8";
		case 0x10: return "AMD K8L";
		case 0x12: return "AMD K10";
		case 0x14: return "AMD Bobcat";
		case 0x15: return "AMD Bulldozer";
		case 0x16: return "AMD Jaguar";
		case 0x17: return "AMD Zen/Zen+/Zen2";
		case 0x18: return "AMD Hygon Dhyana";
		case 0x19: return "AMD Zen 3";
		}
		return STRINGIZE("AMD <unknown 0x" << std::hex << composed_family << ">");
	}

	std::string compute_CPUFamilyName(const char* cpu_vendor, int family, int ext_family) 
	{
		const char* intel_string = "GenuineIntel";
		const char* amd_string = "AuthenticAMD";
		if (LLStringUtil::startsWith(cpu_vendor, intel_string))
		{
			int composed_family = family + ext_family;
			return intel_CPUFamilyName(composed_family);
		}
		else if (LLStringUtil::startsWith(cpu_vendor, amd_string))
		{
			int composed_family = (family == 0xF)
				? family + ext_family
				: family;
			return amd_CPUFamilyName(composed_family);
		}
		return STRINGIZE("Unrecognized CPU vendor <" << cpu_vendor << ">");
	}

} // end unnamed namespace

// The base class for implementations.
// Each platform should override this class.
class LLProcessorInfoImpl
{
public:
	LLProcessorInfoImpl() 
	{
		mProcessorInfo["info"] = LLSD::emptyMap();
		mProcessorInfo["config"] = LLSD::emptyMap();
		mProcessorInfo["extension"] = LLSD::emptyMap();		
	}
	virtual ~LLProcessorInfoImpl() {}

	F64 getCPUFrequency() const 
	{ 
		return getInfo(eFrequency, 0).asReal(); 
	}

	bool hasSSE() const 
	{ 
		return hasExtension(cpu_feature_names[eSSE_Ext]);
	}

	bool hasSSE2() const
	{ 
		return hasExtension(cpu_feature_names[eSSE2_Ext]);
	}

	std::string getCPUFamilyName() const { return getInfo(eFamilyName, "Unset family").asString(); }
	std::string getCPUBrandName() const { return getInfo(eBrandName, "Unset brand").asString(); }

	// This is virtual to support a different linux format.
	// *NOTE:Mani - I didn't want to screw up server use of this data...
	virtual std::string getCPUFeatureDescription() const 
	{
		std::ostringstream out;
		out << std::endl << std::endl;
		out << "// CPU General Information" << std::endl;
		out << "//////////////////////////" << std::endl;
		out << "Processor Name:   " << getCPUBrandName() << std::endl;
		out << "Frequency:        " << getCPUFrequency() << " MHz" << std::endl;
		out << "Vendor:			  " << getInfo(eVendor, "Unset vendor").asString() << std::endl;
		out << "Family:           " << getCPUFamilyName() << " (" << getInfo(eFamily, 0) << ")" << std::endl;
		out << "Extended family:  " << getInfo(eExtendedFamily, 0) << std::endl;
		out << "Model:            " << getInfo(eModel, 0) << std::endl;
		out << "Extended model:   " << getInfo(eExtendedModel, 0) << std::endl;
		out << "Type:             " << getInfo(eType, 0) << std::endl;
		out << "Brand ID:         " << getInfo(eBrandID, 0) << std::endl;
		out << std::endl;
		out << "// CPU Configuration" << std::endl;
		out << "//////////////////////////" << std::endl;
		
		// Iterate through the dictionary of configuration options.
		LLSD configs = mProcessorInfo["config"];
		for(LLSD::map_const_iterator cfgItr = configs.beginMap(); cfgItr != configs.endMap(); ++cfgItr)
		{
			out << cfgItr->first << " = " << cfgItr->second << std::endl;
		}
		out << std::endl;
		
		out << "// CPU Extensions" << std::endl;
		out << "//////////////////////////" << std::endl;
		
		for(LLSD::map_const_iterator itr = mProcessorInfo["extension"].beginMap(); itr != mProcessorInfo["extension"].endMap(); ++itr)
		{
			out << "  " << itr->first << std::endl;			
		}
		return out.str(); 
	}

protected:
	void setInfo(cpu_info info_type, const LLSD& value) 
	{
		setInfo(cpu_info_names[info_type], value);
	}
    LLSD getInfo(cpu_info info_type, const LLSD& defaultVal) const
	{
		return getInfo(cpu_info_names[info_type], defaultVal);
	}

	void setConfig(cpu_config config_type, const LLSD& value) 
	{ 
		setConfig(cpu_config_names[config_type], value);
	}
	LLSD getConfig(cpu_config config_type, const LLSD& defaultVal) const
	{ 
		return getConfig(cpu_config_names[config_type], defaultVal);
	}

	void setExtension(const std::string& name) { mProcessorInfo["extension"][name] = "true"; }
	bool hasExtension(const std::string& name) const
	{ 
		return mProcessorInfo["extension"].has(name);
	}

private:
	void setInfo(const std::string& name, const LLSD& value) { mProcessorInfo["info"][name]=value; }
	LLSD getInfo(const std::string& name, const LLSD& defaultVal) const
	{ 
		if(mProcessorInfo["info"].has(name))
		{
			return mProcessorInfo["info"][name];
		}
		return defaultVal;
	}
	void setConfig(const std::string& name, const LLSD& value) { mProcessorInfo["config"][name]=value; }
	LLSD getConfig(const std::string& name, const LLSD& defaultVal) const
	{ 
		LLSD r = mProcessorInfo["config"].get(name);
		return r.isDefined() ? r : defaultVal;
	}

private:

	LLSD mProcessorInfo;
};




#include <mach/machine.h>
#include <sys/sysctl.h>

class LLProcessorInfoDarwinImpl : public LLProcessorInfoImpl
{
public:
	LLProcessorInfoDarwinImpl() 
	{
		getCPUIDInfo();
		uint64_t frequency = getSysctlInt64("hw.cpufrequency");
		setInfo(eFrequency, (F64)frequency  / (F64)1000000);
	}

	virtual ~LLProcessorInfoDarwinImpl() {}

private:
	int getSysctlInt(const char* name)
   	{
		int result = 0;
		size_t len = sizeof(int);
		int error = sysctlbyname(name, (void*)&result, &len, NULL, 0);
		return error == -1 ? 0 : result;
   	}

	uint64_t getSysctlInt64(const char* name)
   	{
		uint64_t value = 0;
		size_t size = sizeof(value);
		int result = sysctlbyname(name, (void*)&value, &size, NULL, 0);
		if ( result == 0 ) 
		{ 
			if ( size == sizeof( uint64_t ) ) 
				; 
			else if ( size == sizeof( uint32_t ) ) 
				value = (uint64_t)(( uint32_t *)&value); 
			else if ( size == sizeof( uint16_t ) ) 
				value =  (uint64_t)(( uint16_t *)&value); 
			else if ( size == sizeof( uint8_t ) ) 
				value =  (uint64_t)(( uint8_t *)&value); 
			else
			{
				LL_WARNS() << "Unknown type returned from sysctl" << LL_ENDL;
			}
		}
				
		return result == -1 ? 0 : value;
   	}
	
	void getCPUIDInfo()
	{
		size_t len = 0;

		char cpu_brand_string[0x40];
		len = sizeof(cpu_brand_string);
		memset(cpu_brand_string, 0, len);
		sysctlbyname("machdep.cpu.brand_string", (void*)cpu_brand_string, &len, NULL, 0);
		cpu_brand_string[0x3f] = 0;
		setInfo(eBrandName, cpu_brand_string);
		
		char cpu_vendor[0x20];
		len = sizeof(cpu_vendor);
		memset(cpu_vendor, 0, len);
		sysctlbyname("machdep.cpu.vendor", (void*)cpu_vendor, &len, NULL, 0);
		cpu_vendor[0x1f] = 0;
		setInfo(eVendor, cpu_vendor);

		setInfo(eStepping, getSysctlInt("machdep.cpu.stepping"));
		setInfo(eModel, getSysctlInt("machdep.cpu.model"));
		int family = getSysctlInt("machdep.cpu.family");
		int ext_family = getSysctlInt("machdep.cpu.extfamily");
		setInfo(eFamily, family);
		setInfo(eExtendedFamily, ext_family);
		setInfo(eFamilyName, compute_CPUFamilyName(cpu_vendor, family, ext_family));
		setInfo(eExtendedModel, getSysctlInt("machdep.cpu.extmodel"));
		setInfo(eBrandID, getSysctlInt("machdep.cpu.brand"));
		setInfo(eType, 0); // ? where to find this?

		//setConfig(eCLFLUSHCacheLineSize, ((cpu_info[1] >> 8) & 0xff) * 8);
		//setConfig(eAPICPhysicalID, (cpu_info[1] >> 24) & 0xff);
		setConfig(eCacheLineSize, getSysctlInt("machdep.cpu.cache.linesize"));
		setConfig(eL2Associativity, getSysctlInt("machdep.cpu.cache.L2_associativity"));
		setConfig(eCacheSizeK, getSysctlInt("machdep.cpu.cache.size"));
		
		uint64_t feature_info = getSysctlInt64("machdep.cpu.feature_bits");
		S32 *feature_infos = (S32*)(&feature_info);
		
		setConfig(eFeatureBits, feature_infos[0]);

		for(unsigned int index = 0, bit = 1; index < eSSE3_Features; ++index, bit <<= 1)
		{
			if(feature_info & bit)
			{
				setExtension(cpu_feature_names[index]);
			}
		}

		// *NOTE:Mani - I didn't find any docs that assure me that machdep.cpu.feature_bits will always be
		// The feature bits I think it is. Here's a test:
#ifndef LL_RELEASE_FOR_DOWNLOAD
	#if defined(__i386__) && defined(__PIC__)
			/* %ebx may be the PIC register.  */
		#define __cpuid(level, a, b, c, d)			\
		__asm__ ("xchgl\t%%ebx, %1\n\t"			\
				"cpuid\n\t"					\
				"xchgl\t%%ebx, %1\n\t"			\
				: "=a" (a), "=r" (b), "=c" (c), "=d" (d)	\
				: "0" (level))
	#else
		#define __cpuid(level, a, b, c, d)			\
		__asm__ ("cpuid\n\t"					\
				 : "=a" (a), "=b" (b), "=c" (c), "=d" (d)	\
				 : "0" (level))
	#endif

		unsigned int eax, ebx, ecx, edx;
		__cpuid(0x1, eax, ebx, ecx, edx);
		if(feature_infos[0] != (S32)edx)
		{
			LL_ERRS() << "machdep.cpu.feature_bits doesn't match expected cpuid result!" << LL_ENDL;
		} 
#endif // LL_RELEASE_FOR_DOWNLOAD 	


		uint64_t ext_feature_info = getSysctlInt64("machdep.cpu.extfeature_bits");
		S32 *ext_feature_infos = (S32*)(&ext_feature_info);
		setConfig(eExtFeatureBits, ext_feature_infos[0]);
	}
};


//////////////////////////////////////////////////////
// Interface definition
LLProcessorInfo::LLProcessorInfo() : mImpl(NULL)
{
	// *NOTE:Mani - not thread safe.
	if(!mImpl)
	{
		static LLProcessorInfoDarwinImpl the_impl; 
		mImpl = &the_impl;		
	}
}


LLProcessorInfo::~LLProcessorInfo() {}
F64MegahertzImplicit LLProcessorInfo::getCPUFrequency() const { return mImpl->getCPUFrequency(); }
bool LLProcessorInfo::hasSSE() const { return mImpl->hasSSE(); }
bool LLProcessorInfo::hasSSE2() const { return mImpl->hasSSE2(); }
std::string LLProcessorInfo::getCPUFamilyName() const { return mImpl->getCPUFamilyName(); }
std::string LLProcessorInfo::getCPUBrandName() const { return mImpl->getCPUBrandName(); }
std::string LLProcessorInfo::getCPUFeatureDescription() const { return mImpl->getCPUFeatureDescription(); }

