/** 
 * @file llsys.cpp
 * @brief Implementation of the basic system query functions.
 *
 * $LicenseInfo:firstyear=2002&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */


#include "linden_common.h"

#include "llsys.h"

#include <iostream>
#ifdef LL_USESYSTEMLIBS
# include <zlib.h>
#else
# include "zlib/zlib.h"
#endif

#include "llprocessor.h"
#include "llerrorcontrol.h"
#include "llevents.h"
#include "llformat.h"
#include "llregex.h"
#include "lltimer.h"
#include "llsdserialize.h"
#include "llsdutil.h"
#include <boost/bind.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_float.hpp>

using namespace llsd;
#   include "llsys_objc.h"  // <AV:CR>
#	include <errno.h>
#	include <sys/sysctl.h>
#	include <sys/utsname.h>
#	include <stdint.h>
#	include <CoreServices/CoreServices.h>
#   include <stdexcept>
#	include <mach/host_info.h>
#	include <mach/mach_host.h>
#	include <mach/task.h>
#	include <mach/task_info.h>

LLCPUInfo gSysCPU;

// Don't log memory info any more often than this. It also serves as our
// framerate sample size.
static const F32 MEM_INFO_THROTTLE = 20;
// Sliding window of samples. We intentionally limit the length of time we
// remember "the slowest" framerate because framerate is very slow at login.
// If we only triggered FrameWatcher logging when the session framerate
// dropped below the login framerate, we'd have very little additional data.
static const F32 MEM_INFO_WINDOW = 10*60;

LLOSInfo::LLOSInfo() :
	mMajorVer(0), mMinorVer(0), mBuild(0), mOSVersionString("")	 
{	
	// Initialize mOSStringSimple to something like:
	// "Mac OS X 10.6.7"
	{
	// <AV:CR>

		const char * DARWIN_PRODUCT_NAME = "macOS";
		
		S32 major_version, minor_version, bugfix_version = 0;

		if (LLSysDarwin::getOperatingSystemInfo(major_version, minor_version, bugfix_version))
		{
			mMajorVer = major_version;
			mMinorVer = minor_version;
			mBuild = bugfix_version;

			std::stringstream os_version_string;
			os_version_string << DARWIN_PRODUCT_NAME << " " << mMajorVer << "." << mMinorVer << "." << mBuild;
			
			// Put it in the OS string we are compiling
			mOSStringSimple.append(os_version_string.str());
		}
		else
		{
			mOSStringSimple.append("Unable to collect OS info");
		}
		// </AV:CR>
	}
	
	// Initialize mOSString to something like:
	// "Mac OS X 10.6.7 Darwin Kernel Version 10.7.0: Sat Jan 29 15:17:16 PST 2011; root:xnu-1504.9.37~1/RELEASE_I386 i386"
	struct utsname un;
	if(uname(&un) != -1)
	{		
		mOSString = mOSStringSimple;
		mOSString.append(" ");
		mOSString.append(un.sysname);
		mOSString.append(" ");
		mOSString.append(un.release);
		mOSString.append(" ");
		mOSString.append(un.version);
		mOSString.append(" ");
		mOSString.append(un.machine);
	}
	else
	{
		mOSString = mOSStringSimple;
	}
	

	std::stringstream dotted_version_string;
	dotted_version_string << mMajorVer << "." << mMinorVer << "." << mBuild;
	mOSVersionString.append(dotted_version_string.str());

}

// static
long LLOSInfo::getMaxOpenFiles()
{
	const long OPEN_MAX_GUESS = 256;

#ifdef	OPEN_MAX
	static long open_max = OPEN_MAX;
#else
	static long open_max = 0;
#endif

	if (0 == open_max)
	{
		// First time through.
		errno = 0;
		if ( (open_max = sysconf(_SC_OPEN_MAX)) < 0)
		{
			if (0 == errno)
			{
				// Indeterminate.
				open_max = OPEN_MAX_GUESS;
			}
			else
			{
				LL_ERRS() << "LLOSInfo::getMaxOpenFiles: sysconf error for _SC_OPEN_MAX" << LL_ENDL;
			}
		}
	}
	return open_max;
}

void LLOSInfo::stream(std::ostream& s) const
{
	s << mOSString;
}

const std::string& LLOSInfo::getOSString() const
{
	return mOSString;
}

const std::string& LLOSInfo::getOSStringSimple() const
{
	return mOSStringSimple;
}

const std::string& LLOSInfo::getOSVersionString() const
{
	return mOSVersionString;
}

//static
U32 LLOSInfo::getProcessVirtualSizeKB()
{
	U32 virtual_size = 0;
	return virtual_size;
}

//static
U32 LLOSInfo::getProcessResidentSizeKB()
{
	U32 resident_size = 0;
	return resident_size;
}

LLCPUInfo::LLCPUInfo()
{
	std::ostringstream out;
	LLProcessorInfo proc;
	// proc.WriteInfoTextFile("procInfo.txt");
	mHasSSE = proc.hasSSE();
	mHasSSE2 = proc.hasSSE2();
	mCPUMHz = (F64)proc.getCPUFrequency();
	mFamily = proc.getCPUFamilyName();
	mCPUString = "Unknown";

	out << proc.getCPUBrandName();
	if (200 < mCPUMHz && mCPUMHz < 10000)           // *NOTE: cpu speed is often way wrong, do a sanity check
	{
		out << " (" << mCPUMHz << " MHz)";
	}
	mCPUString = out.str();
	LLStringUtil::trim(mCPUString);
}

bool LLCPUInfo::hasSSE() const
{
	return mHasSSE;
}

bool LLCPUInfo::hasSSE2() const
{
	return mHasSSE2;
}

F64 LLCPUInfo::getMHz() const
{
	return mCPUMHz;
}

std::string LLCPUInfo::getCPUString() const
{
	return mCPUString;
}

void LLCPUInfo::stream(std::ostream& s) const
{
	// gather machine information.
	s << LLProcessorInfo().getCPUFeatureDescription();

	// These are interesting as they reflect our internal view of the
	// CPU's attributes regardless of platform
	s << "->mHasSSE:     " << (U32)mHasSSE << std::endl;
	s << "->mHasSSE2:    " << (U32)mHasSSE2 << std::endl;
	s << "->mCPUMHz:     " << mCPUMHz << std::endl;
	s << "->mCPUString:  " << mCPUString << std::endl;
}

// Helper class for LLMemoryInfo: accumulate stats in the form we store for
// LLMemoryInfo::getStatsMap().
class Stats
{
public:
	Stats():
		mStats(LLSD::emptyMap())
	{}

	// Store every integer type as LLSD::Integer.
	template <class T>
	void add(const LLSD::String& name, const T& value,
			 typename boost::enable_if<boost::is_integral<T> >::type* = 0)
	{
		mStats[name] = LLSD::Integer(value);
	}

	// Store every floating-point type as LLSD::Real.
	template <class T>
	void add(const LLSD::String& name, const T& value,
			 typename boost::enable_if<boost::is_float<T> >::type* = 0)
	{
		mStats[name] = LLSD::Real(value);
	}

	// Hope that LLSD::Date values are sufficiently unambiguous.
	void add(const LLSD::String& name, const LLSD::Date& value)
	{
		mStats[name] = value;
	}

	LLSD get() const { return mStats; }

private:
	LLSD mStats;
};

LLMemoryInfo::LLMemoryInfo()
{
	refresh();
}


U32Kilobytes LLMemoryInfo::getPhysicalMemoryKB() const
{
	// This might work on Linux as well.  Someone check...
	uint64_t phys = 0;
	int mib[2] = { CTL_HW, HW_MEMSIZE };

	size_t len = sizeof(phys);	
	sysctl(mib, 2, &phys, &len, NULL, 0);
	
	return U64Bytes(phys);
}

//static
void LLMemoryInfo::getAvailableMemoryKB(U32Kilobytes& avail_physical_mem_kb, U32Kilobytes& avail_virtual_mem_kb)
{
	// mStatsMap is derived from vm_stat, look for (e.g.) "kb free":
	// $ vm_stat
	// Mach Virtual Memory Statistics: (page size of 4096 bytes)
	// Pages free:                   462078.
	// Pages active:                 142010.
	// Pages inactive:               220007.
	// Pages wired down:             159552.
	// "Translation faults":      220825184.
	// Pages copy-on-write:         2104153.
	// Pages zero filled:         167034876.
	// Pages reactivated:             65153.
	// Pageins:                     2097212.
	// Pageouts:                      41759.
	// Object cache: 841598 hits of 7629869 lookups (11% hit rate)
	avail_physical_mem_kb = (U32Kilobytes)-1 ;
	avail_virtual_mem_kb = (U32Kilobytes)-1 ;
}

void LLMemoryInfo::stream(std::ostream& s) const
{
	// We want these memory stats to be easy to grep from the log, along with
	// the timestamp. So preface each line with the timestamp and a
	// distinctive marker. Without that, we'd have to search the log for the
	// introducer line, then read subsequent lines, etc...
	std::string pfx(LLError::utcTime() + " <mem> ");

	// Max key length
	size_t key_width(0);
	BOOST_FOREACH(const MapEntry& pair, inMap(mStatsMap))
	{
		size_t len(pair.first.length());
		if (len > key_width)
		{
			key_width = len;
		}
	}

	// Now stream stats
	BOOST_FOREACH(const MapEntry& pair, inMap(mStatsMap))
	{
		s << pfx << std::setw(int(key_width+1)) << (pair.first + ':') << ' ';
		LLSD value(pair.second);
		if (value.isInteger())
			s << std::setw(12) << value.asInteger();
		else if (value.isReal())
			s << std::fixed << std::setprecision(1) << value.asReal();
		else if (value.isDate())
			value.asDate().toStream(s);
		else
			s << value;           // just use default LLSD formatting
		s << std::endl;
	}
}

LLSD LLMemoryInfo::getStatsMap() const
{
	return mStatsMap;
}

LLMemoryInfo& LLMemoryInfo::refresh()
{
	mStatsMap = loadStatsMap();

	LL_DEBUGS("LLMemoryInfo") << "Populated mStatsMap:\n";
	LLSDSerialize::toPrettyXML(mStatsMap, LL_CONT);
	LL_ENDL;

	return *this;
}

LLSD LLMemoryInfo::loadStatsMap()
{
	// This implementation is derived from stream() code (as of 2011-06-29).
	Stats stats;

	// associate timestamp for analysis over time
	stats.add("timestamp", LLDate::now());

	const vm_size_t pagekb(vm_page_size / 1024);
	
	//
	// Collect the vm_stat's
	//
	
	{
		vm_statistics64_data_t vmstat;
		mach_msg_type_number_t vmstatCount = HOST_VM_INFO64_COUNT;

		if (host_statistics64(mach_host_self(), HOST_VM_INFO64, (host_info64_t) &vmstat, &vmstatCount) != KERN_SUCCESS)
	{
			LL_WARNS("LLMemoryInfo") << "Unable to collect memory information" << LL_ENDL;
		}
		else
		{
			stats.add("Pages free KB",		pagekb * vmstat.free_count);
			stats.add("Pages active KB",	pagekb * vmstat.active_count);
			stats.add("Pages inactive KB",	pagekb * vmstat.inactive_count);
			stats.add("Pages wired KB",		pagekb * vmstat.wire_count);

			stats.add("Pages zero fill",		vmstat.zero_fill_count);
			stats.add("Page reactivations",		vmstat.reactivations);
			stats.add("Page-ins",				vmstat.pageins);
			stats.add("Page-outs",				vmstat.pageouts);
			
			stats.add("Faults",					vmstat.faults);
			stats.add("Faults copy-on-write",	vmstat.cow_faults);
			
			stats.add("Cache lookups",			vmstat.lookups);
			stats.add("Cache hits",				vmstat.hits);
			
			stats.add("Page purgeable count",	vmstat.purgeable_count);
			stats.add("Page purges",			vmstat.purges);
			
			stats.add("Page speculative reads",	vmstat.speculative_count);
		}
	}

	//
	// Collect the misc task info
	//

		{
		task_events_info_data_t taskinfo;
		unsigned taskinfoSize = sizeof(taskinfo);
		
		if (task_info(mach_task_self(), TASK_EVENTS_INFO, (task_info_t) &taskinfo, &taskinfoSize) != KERN_SUCCESS)
					{
			LL_WARNS("LLMemoryInfo") << "Unable to collect task information" << LL_ENDL;
			}
			else
			{
			stats.add("Task page-ins",					taskinfo.pageins);
			stats.add("Task copy-on-write faults",		taskinfo.cow_faults);
			stats.add("Task messages sent",				taskinfo.messages_sent);
			stats.add("Task messages received",			taskinfo.messages_received);
			stats.add("Task mach system call count",	taskinfo.syscalls_mach);
			stats.add("Task unix system call count",	taskinfo.syscalls_unix);
			stats.add("Task context switch count",		taskinfo.csw);
			}
	}	
	
	//
	// Collect the basic task info
	//

		{
			mach_task_basic_info_data_t taskinfo;
			mach_msg_type_number_t task_count = MACH_TASK_BASIC_INFO_COUNT;
			if (task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t) &taskinfo, &task_count) != KERN_SUCCESS)
			{
				LL_WARNS("LLMemoryInfo") << "Unable to collect task information" << LL_ENDL;
			}
			else
			{
				stats.add("Basic virtual memory KB", taskinfo.virtual_size / 1024);
				stats.add("Basic resident memory KB", taskinfo.resident_size / 1024);
				stats.add("Basic max resident memory KB", taskinfo.resident_size_max / 1024);
				stats.add("Basic new thread policy", taskinfo.policy);
				stats.add("Basic suspend count", taskinfo.suspend_count);
			}
	}
	
	return stats.get();
}

std::ostream& operator<<(std::ostream& s, const LLOSInfo& info)
{
	info.stream(s);
	return s;
}

std::ostream& operator<<(std::ostream& s, const LLCPUInfo& info)
{
	info.stream(s);
	return s;
}

std::ostream& operator<<(std::ostream& s, const LLMemoryInfo& info)
{
	info.stream(s);
	return s;
}

class FrameWatcher
{
public:
    FrameWatcher():
        // Hooking onto the "mainloop" event pump gets us one call per frame.
        mConnection(LLEventPumps::instance()
                    .obtain("mainloop")
                    .listen("FrameWatcher", boost::bind(&FrameWatcher::tick, this, _1))),
        // Initializing mSampleStart to an invalid timestamp alerts us to skip
        // trying to compute framerate on the first call.
        mSampleStart(-1),
        // Initializing mSampleEnd to 0 ensures that we treat the first call
        // as the completion of a sample window.
        mSampleEnd(0),
        mFrames(0),
        // Both MEM_INFO_WINDOW and MEM_INFO_THROTTLE are in seconds. We need
        // the number of integer MEM_INFO_THROTTLE sample slots that will fit
        // in MEM_INFO_WINDOW. Round up.
        mSamples(u_long((MEM_INFO_WINDOW / MEM_INFO_THROTTLE) + 0.7)),
        // Initializing to F32_MAX means that the first real frame will become
        // the slowest ever, which sounds like a good idea.
        mSlowest(F32_MAX)
    {}

    bool tick(const LLSD&)
    {
        F32 timestamp(mTimer.getElapsedTimeF32());

        // Count this frame in the interval just completed.
        ++mFrames;

        // Have we finished a sample window yet?
        if (timestamp < mSampleEnd)
        {
            // no, just keep waiting
            return false;
        }

        // Set up for next sample window. Capture values for previous frame in
        // local variables and reset data members.
        U32 frames(mFrames);
        F32 sampleStart(mSampleStart);
        // No frames yet in next window
        mFrames = 0;
        // which starts right now
        mSampleStart = timestamp;
        // and ends MEM_INFO_THROTTLE seconds in the future
        mSampleEnd = mSampleStart + MEM_INFO_THROTTLE;

        // On the very first call, that's all we can do, no framerate
        // computation is possible.
        if (sampleStart < 0)
        {
            return false;
        }

        // How long did this actually take? As framerate slows, the duration
        // of the frame we just finished could push us WELL beyond our desired
        // sample window size.
        F32 elapsed(timestamp - sampleStart);
        F32 framerate(frames/elapsed);

        // Remember previous slowest framerate because we're just about to
        // update it.
        F32 slowest(mSlowest);
        // Remember previous number of samples.
        boost::circular_buffer<F32>::size_type prevSize(mSamples.size());

        // Capture new framerate in our samples buffer. Once the buffer is
        // full (after MEM_INFO_WINDOW seconds), this will displace the oldest
        // sample. ("So they all rolled over, and one fell out...")
        mSamples.push_back(framerate);

        // Calculate the new minimum framerate. I know of no way to update a
        // rolling minimum without ever rescanning the buffer. But since there
        // are only a few tens of items in this buffer, rescanning it is
        // probably cheaper (and certainly easier to reason about) than
        // attempting to optimize away some of the scans.
        mSlowest = framerate;       // pick an arbitrary entry to start
        for (boost::circular_buffer<F32>::const_iterator si(mSamples.begin()), send(mSamples.end());
             si != send; ++si)
        {
            if (*si < mSlowest)
            {
                mSlowest = *si;
            }
        }

        // We're especially interested in memory as framerate drops. Only log
        // when framerate drops below the slowest framerate we remember.
        // (Should always be true for the end of the very first sample
        // window.)
        if (framerate >= slowest)
        {
            return false;
        }
        // Congratulations, we've hit a new low.  :-P

        LL_INFOS("FrameWatcher") << ' ';
        if (! prevSize)
        {
            LL_CONT << "initial framerate ";
        }
        else
        {
            LL_CONT << "slowest framerate for last " << int(prevSize * MEM_INFO_THROTTLE)
                    << " seconds ";
        }

	S32 precision = LL_CONT.precision();

        LL_CONT << std::fixed << std::setprecision(1) << framerate << '\n'
                << LLMemoryInfo();

	LL_CONT.precision(precision);
	LL_CONT << LL_ENDL;
        return false;
    }

private:
    // Storing the connection in an LLTempBoundListener ensures it will be
    // disconnected when we're destroyed.
    LLTempBoundListener mConnection;
    // Track elapsed time
    LLTimer mTimer;
    // Some of what you see here is in fact redundant with functionality you
    // can get from LLTimer. Unfortunately the LLTimer API is missing the
    // feature we need: has at least the stated interval elapsed, and if so,
    // exactly how long has passed? So we have to do it by hand, sigh.
    // Time at start, end of sample window
    F32 mSampleStart, mSampleEnd;
    // Frames this sample window
    U32 mFrames;
    // Sliding window of framerate samples
    boost::circular_buffer<F32> mSamples;
    // Slowest framerate in mSamples
    F32 mSlowest;
};

// Need an instance of FrameWatcher before it does any good
static FrameWatcher sFrameWatcher;

bool gunzip_file(const std::string& srcfile, const std::string& dstfile)
{
	std::string tmpfile;
	const S32 UNCOMPRESS_BUFFER_SIZE = 32768;
	bool retval = false;
	gzFile src = NULL;
	U8 buffer[UNCOMPRESS_BUFFER_SIZE];
	LLFILE *dst = NULL;
	S32 bytes = 0;
	tmpfile = dstfile + ".t";
	src = gzopen(srcfile.c_str(), "rb");
	if (! src) goto err;
	dst = LLFile::fopen(tmpfile, "wb");		/* Flawfinder: ignore */
	if (! dst) goto err;
	do
	{
		bytes = gzread(src, buffer, UNCOMPRESS_BUFFER_SIZE);
		size_t nwrit = fwrite(buffer, sizeof(U8), bytes, dst);
		if (nwrit < (size_t) bytes)
		{
			LL_WARNS() << "Short write on " << tmpfile << ": Wrote " << nwrit << " of " << bytes << " bytes." << LL_ENDL;
			goto err;
		}
	} while(gzeof(src) == 0);
	fclose(dst); 
	dst = NULL;	
	if (LLFile::rename(tmpfile, dstfile) == -1) goto err;		/* Flawfinder: ignore */
	retval = true;
err:
	if (src != NULL) gzclose(src);
	if (dst != NULL) fclose(dst);
	return retval;
}

bool gzip_file(const std::string& srcfile, const std::string& dstfile)
{
	const S32 COMPRESS_BUFFER_SIZE = 32768;
	std::string tmpfile;
	bool retval = false;
	U8 buffer[COMPRESS_BUFFER_SIZE];
	gzFile dst = NULL;
	LLFILE *src = NULL;
	S32 bytes = 0;
	tmpfile = dstfile + ".t";
	dst = gzopen(tmpfile.c_str(), "wb");		/* Flawfinder: ignore */
	if (! dst) goto err;
	src = LLFile::fopen(srcfile, "rb");		/* Flawfinder: ignore */
	if (! src) goto err;

	while ((bytes = (S32)fread(buffer, sizeof(U8), COMPRESS_BUFFER_SIZE, src)) > 0)
	{
		if (gzwrite(dst, buffer, bytes) <= 0)
		{
			LL_WARNS() << "gzwrite failed: " << gzerror(dst, NULL) << LL_ENDL;
			goto err;
		}
	}

	if (ferror(src))
	{
		LL_WARNS() << "Error reading " << srcfile << LL_ENDL;
		goto err;
	}

	gzclose(dst);
	dst = NULL;

	if (LLFile::rename(tmpfile, dstfile) == -1) goto err;		/* Flawfinder: ignore */
	retval = true;
 err:
	if (src != NULL) fclose(src);
	if (dst != NULL) gzclose(dst);
	return retval;
}

